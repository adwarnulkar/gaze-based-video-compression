%% Segmentation (Algorithm 1)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% Email address: warnulka@usc.edu  
% April 2017; Last revision: 11-April-2017
%
% Algorithm
%
% Without blob reshaping
% Without frame correction

clc; clear ; close all;

folderName = 'TA';
imageName  = 'frame';
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;

% Image parameters
width    = 960;
height   = 544;
n_frames = 5;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

for i = 1:n_frames-2
    
    str = sprintf('Frame number %d',i);
    imgINumber = i;
    imgPNumber = i+2;
    
    imgIFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgINumber);
    imgPFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgPNumber);
    
    imgI = double(rgb2gray(imread(imgIFile)));
    imgP = double(rgb2gray(imread(imgPFile)));
    
    label = segmentFB( imgI , imgP , mbSize, p , threshold , iteration );
    blockRatio = mbSize / 8;
    label_new = replicateBlock(label,blockRatio);
    
    imshow(label_new,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    pause(1);
    
end

%% Segmentation (Algorithm 2)
% With blob reshaping
% Without frame correction
%
% Date: 24th April 2017

clc; clear ; close all;

folderName = 'TA';
imageName  = 'frame';
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;

% Image parameters
width    = 960;
height   = 544;
n_frames = 15;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

figBlob = figure('Name','Without Blob detection');
figRect = figure('Name','With Blob detection');

for i = 1:n_frames-2
    
    str = sprintf('Frame number %d',i);
    imgINumber = i;
    imgPNumber = i+2;
    
    imgIFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgINumber);
    imgPFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgPNumber);
    
    imgI = double(rgb2gray(imread(imgIFile)));
    imgP = double(rgb2gray(imread(imgPFile)));
    
    label = segmentFB( imgI , imgP , mbSize, threshold , iteration );
    label_temp = label;
    
    % Bounding a rectangle around the blob
    [labeledImage, numBlobs] = bwlabel(label);
    measurements = regionprops(labeledImage, 'BoundingBox');
    
    for k = 1:1:numBlobs
        xLeft = ceil(measurements(k).BoundingBox(1));
        xRight = xLeft + measurements(k).BoundingBox(3);
        yTop = ceil(measurements(k).BoundingBox(2));
        yBottom = yTop + measurements(k).BoundingBox(4);
        label_temp(yTop:yBottom , xLeft:xRight) = 1;
    end
    
    figure(figBlob);
    imshow(label,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    figure(figRect)
    imshow(label_temp,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    pause(1);
    
end

%% Segmentation (Algorithm 3)
% With blob reshaping
% With frame correction
%
% Date: 24th April 2017

clc; clear ; close all;

folderName = 'TA';
imageName  = 'frame';
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;
foreGroundThreshold = 5;

% Image parameters
width    = 960;
height   = 544;
n_frames = 10;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

figBlob = figure('Name','Without Blob detection');
figRect = figure('Name','With Blob detection');

for i = 1:n_frames-2
    
    str = sprintf('Frame number %d',i);
    imgINumber = i;
    imgPNumber = i+2;
    
    imgIFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgINumber);
    imgPFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgPNumber);
    
    imgI = double(rgb2gray(imread(imgIFile)));
    imgP = double(rgb2gray(imread(imgPFile)));
    
    label = segmentFB( imgI , imgP , mbSize, threshold , iteration );
    label_temp = label;
    
    if (sum(label(:)) < foreGroundThreshold);
        label_temp = label_previous;
    else
        
        % Bounding a rectangle around the blob
        [labeledImage, numBlobs] = bwlabel(label);
        measurements = regionprops(labeledImage, 'BoundingBox');
        
        for k = 1:1:numBlobs
            xLeft = ceil(measurements(k).BoundingBox(1));
            xRight = xLeft + measurements(k).BoundingBox(3);
            yTop = ceil(measurements(k).BoundingBox(2));
            yBottom = yTop + measurements(k).BoundingBox(4);
            label_temp(yTop:yBottom , xLeft:xRight) = 1;
        end
        
    end
    
    % Buffer the label frame
    label_previous = label_temp;
    
    figure(figBlob);
    imshow(label,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    figure(figRect)
    imshow(label_temp,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    pause(1);
    
end

%% Segmentation (Algorithm 4) (Frame by Frame writing)
% With blob reshaping
% Without frame correction
% Frame by Frame writing (4 files)
% Date: 24th April 2017

clc; clear ; close all;

folderName = 'TA';
imageName  = 'frame';
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;

% Image parameters
width    = 960;
height   = 544;
n_frames = 15;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

figBlob = figure('Name','Without Blob detection');
figRect = figure('Name','With Blob detection');

for i = 1:n_frames-2
    
    str = sprintf('Frame number %d',i);
    imgINumber = i;
    imgPNumber = i+2;
    
    imgIFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgINumber);
    imgPFile = sprintf('./%s/%s_%d.ras',folderName, imageName, imgPNumber);
    
    imgI = double(rgb2gray(imread(imgIFile)));
    imgP = double(rgb2gray(imread(imgPFile)));
    
    label = segmentFB( imgI , imgP , mbSize, threshold , iteration );
    label_temp = label;
    
    % Bounding a rectangle around the blob
    [labeledImage, numBlobs] = bwlabel(label);
    measurements = regionprops(labeledImage, 'BoundingBox');
    
    for k = 1:1:numBlobs
        xLeft = ceil(measurements(k).BoundingBox(1));
        xRight = xLeft + measurements(k).BoundingBox(3);
        yTop = ceil(measurements(k).BoundingBox(2));
        yBottom = yTop + measurements(k).BoundingBox(4);
        label_temp(yTop:yBottom , xLeft:xRight) = 1;
    end
    
    figure(figBlob);
    imshow(label,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    figure(figRect)
    imshow(label_temp,'InitialMagnification','fit');
    title_str = sprintf('Frame Number %d',i+2);
    title(title_str);
    
    pause(1);
    
end


