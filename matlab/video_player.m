%% Raw Video Player (Full file read at once)

clc ; clear ; close all;

video_file = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');

width = 960;
height = 540;
loops = 1;
fps = 10;

[n_frame , frame_process] = video_player_raw( video_file , width , height , fps ,loops);

% Frame process time analysis
figure('Name','Frame Process Time');
stem(1:n_frame, frame_process.*1000); grid on;
xlabel('Frame number');
ylabel('Frame process time');

%% Raw Video player (Read frame by frame)

clc ; clear ; close all;

video_file = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');

width = 960;
height = 540;
loops = 1;
fps = 20;
n_frame = 360;
loops = 1;
A = fread(video_file,960*540*3,'uint8');
frame_time_sec = 1/fps;               % Time per frame 


% RGB plane initialization
R = zeros(height,width);
G = zeros(height,width);
B = zeros(height,width);

%pause(15)

pointer = 1;
for k = 1:loops
    for n = 1:1:n_frame
        A = fread(video_file,960*540*3,'uint8');
        tic
        for y = 1:1:height
            for x = 1:1:width
                R(y,x) = A(pointer);
                G(y,x) = A(pointer + width*height);
                B(y,x) = A(pointer + 2*width*height);
                pointer = pointer + 1;
            end
        end
    
        R = uint8(R);
        G = uint8(G);
        B = uint8(B);
        rgb = cat(3,R,G,B);
        frame_process(n) = toc;
        pause(frame_time_sec-frame_process);
        imshow(rgb,'InitialMagnification','fit');
        %pointer = pointer + 2*width*height;
        pointer = 1;

    end

end

% Frame process time analysis
figure('Name','Frame Process Time');
stem(1:n_frame, frame_process.*1000); grid on;
xlabel('Frame number');
ylabel('Frame process time');

%% Raw Video player (One HOT display)

clc ; clear ; close all;

%video_file = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');
%video_file = fopen('/Users/adwarnulkar/Downloads/test_cases/two_people.rgb');
video_file = fopen('/Users/adwarnulkar/Downloads/test_cases/two_people_moving_background.rgb');

width = 960;
height = 540;
loops = 1;
fps = 30;
n_frame = 300;
loops = 1;
A = fread(video_file,960*540*3,'uint8');
frame_time_sec = 1/fps;               % Time per frame 

% RGB plane initialization
R = zeros(height,width,n_frame);
G = zeros(height,width,n_frame);
B = zeros(height,width,n_frame);

%pause(15)

pointer = 1;
for k = 1:loops
    for n = 1:1:n_frame
        A = fread(video_file,960*540*3,'uint8');
        
        for y = 1:1:height
            for x = 1:1:width
                R(y,x,n) = A(pointer);
                G(y,x,n) = A(pointer + width*height);
                B(y,x,n) = A(pointer + 2*width*height);
                pointer = pointer + 1;
            end
        end
    
        R = uint8(R);
        G = uint8(G);
        B = uint8(B);
        pointer = 1;

    end
    
    R = uint8(R);
    G = uint8(G);
    B = uint8(B);

end

fclose(video_file);

% Display the video
clear A;
figure('Name','Video');
for n = 1:1:n_frame
    tic
    rgb = cat(3,R(:,:,n),G(:,:,n),B(:,:,n));
    frame_process(n) = toc;
    pause(frame_time_sec-frame_process(n));
    %tic
    imshow(rgb,'InitialMagnification','fit');
    %imshow_time(n) = toc;
end
    
% Frame process time analysis
figure('Name','Frame Process Time');
stem(1:n_frame, frame_process.*1000,'LineWidth',2.0); grid on;
xlabel('Frame number');
ylabel('Frame process time (ms)');

%% Raw Video player (Read frame by frame and store it)

clc ; clear ; close all;

video_file = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');

width = 960;
height = 540;
loops = 1;
n_frame = 30;
loops = 1;
A = fread(video_file,960*540*3,'uint8');

folder = 'TA';

% RGB plane initialization
R = zeros(544,width);
G = zeros(544,width);
B = zeros(544,width);

%pause(15)

pointer = 1;
for k = 1:loops
    for n = 1:1:n_frame
        
        imgname = sprintf('./%s/frame_%d.ras',folder,n);
        
        A = fread(video_file,960*540*3,'uint8');

        for y = 1:1:height
            for x = 1:1:width
                R(y,x) = A(pointer);
                G(y,x) = A(pointer + width*height);
                B(y,x) = A(pointer + 2*width*height);
                pointer = pointer + 1;
            end
        end
        
        % Replicate last 4 lines
        R(541:544,:) = repmat(R(540,:),4,1);
        G(541:544,:) = repmat(G(540,:),4,1);
        B(541:544,:) = repmat(B(540,:),4,1);
    
        R = uint8(R);
        G = uint8(G);
        B = uint8(B);
        rgb = cat(3,R,G,B);
        imwrite(rgb,imgname);

        pointer = 1;

    end

end

