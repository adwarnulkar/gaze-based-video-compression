%% Encode (Video) (With Bounding Box , write each frames)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu
% May 2017; Last revision: 03-May-2017
%
% Algorithm:
%
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Convert DCT doefficients from float to int
% * Store the DCT coefficients of all the block in a file (bit stream)
% * create encoded files for each frame
% * Detect foreground and background
% * This file (encoder.dct) will be used by the decoder.
%
% * Read from one file and write into multiple

clc ; clear ; close all;

% Reading the video (frame by frame)
file_name = '/Users/adwarnulkar/Downloads/test_cases/oneperson_960_540.rgb';
%file_name = '/Users/adwarnulkar/Downloads/test_cases/two_people.rgb';
%file_name = '/Users/adwarnulkar/Downloads/test_cases/two_people_moving_background.rgb';
video_file = fopen(file_name);

% Video parameters
width  = 960;
height = 540;
height_new = 544;
block_height = 8;
block_width  = 8;

% Calculate the number of frames
s = dir(file_name);
n_frame = (s.bytes)/(width*height*3);
%n_frame = 363; % Uncomment this line for lesser frames
fprintf('Number of frame: %d\n',n_frame);

folder_name = sprintf('SESTSS_%d',n_frame);
mkdir(folder_name);

% Segmentation parameters
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;
blockRatio = mbSize / 8;

% Extract image planes from the byte stream
frame_process = zeros(1,n_frame);
frame_segment = zeros(1,n_frame);
frame_read    = zeros(1,n_frame);
frame_write   = zeros(1,n_frame);

% Initialize the frame buffers
label_new = zeros(544/8 , 960/8);
R = zeros(height_new,width);
G = zeros(height_new,width);
B = zeros(height_new,width);
R_DCT = zeros(8,8);
G_DCT = zeros(8,8);
B_DCT = zeros(8,8);
T = dctmtx(8);

for frame = 1:1:n_frame
    
    str = sprintf('./%s/frame_%d.dct',folder_name,frame);
    fileID = fopen(str,'w');

    tic    
    % Reading current frame
    A = fread(video_file,960*540*3,'uint8');
    %frame_read(frame) = toc;
    
    pointer = 1; height = 540; % Revert back for reading frame
    for x = 1:1:height
        for y = 1:1:width
            R(x,y) = A(pointer);
            G(x,y) = A(pointer + width*height);
            B(x,y) = A(pointer + 2*width*height);
            pointer = pointer + 1;
        end
    end
    
    %Line replication logic (If image is not divisible into 8x8 blocks)
    height = 544; % Nearest Multiple of 8
    R(541:544,:) = repmat(R(540,:),4,1);
    G(541:544,:) = repmat(G(540,:),4,1);
    B(541:544,:) = repmat(B(540,:),4,1);
    
    % Frame buffering logic
    % I_frame is n-2 frame
    % I_frame_buf is n-1 frame which will be I_Frame in next iteration
    % P_frame is the n frame
    if (frame == 1) % No need to perform segmentation
        label = zeros(height/mbSize,width/mbSize);
        I_frame = double(rgb2gray(cat(3,uint8(R),uint8(G),uint8(B))));
    elseif (frame == 2)
        label = zeros(height/mbSize,width/mbSize);
        I_frame_buf = double(rgb2gray(cat(3,uint8(R),uint8(G),uint8(B))));
    else
        P_frame = double(rgb2gray(cat(3,uint8(R),uint8(G),uint8(B))));
        % Segmentation logic goes here
        [label] = segmentFB( I_frame , P_frame , mbSize, p , threshold , iteration );
    end
    
    % Bounding a rectangle around the blob
    [labeledImage, numBlobs] = bwlabel(label);
    measurements = regionprops(labeledImage, 'BoundingBox');
    
    for k = 1:1:numBlobs
        xLeft = ceil(measurements(k).BoundingBox(1));
        xRight = xLeft + measurements(k).BoundingBox(3);
        yTop = ceil(measurements(k).BoundingBox(2));
        yBottom = yTop + measurements(k).BoundingBox(4);
        label(yTop:yBottom , xLeft:xRight) = 1;
    end
    
    % Scaling the label from 16x16 to 8x8 blocks
    if(sum(sum(label)) ~= 0)
        label_new = replicateBlock( label , blockRatio );
    end
    
    % Reassign buffers after segmentation
    if (frame > 2)
        I_frame = I_frame_buf;
        I_frame_buf = P_frame;
    end
    
    % Divide image into 8x8 blocks and perform DCT
    for x = 1:8:height
        for y = 1:8:width  
           
            FB_type = label_new(floor(x/8 + 1),floor(y/8 + 1));
            fwrite(fileID,FB_type,'int16');
            
            R_DCT = T*R(x:x+8-1 , y:y+8-1)*T';
            G_DCT = T*G(x:x+8-1 , y:y+8-1)*T';
            B_DCT = T*B(x:x+8-1 , y:y+8-1)*T';
            
            fwrite(fileID,R_DCT','int16');
            fwrite(fileID,G_DCT','int16');
            fwrite(fileID,B_DCT','int16');
   
        end
    end
    
    fclose(fileID);
    
    frame_segment(frame) = toc;
    
%     imshow(label_new,'InitialMagnification','fit');
%     title_str = sprintf('Frame number %d',frame);
%     title(title_str);
%     pause(1);
    
    fprintf('Completed Processing frame %d\n',frame);
end

fclose(video_file);

fprintf('....\n');
fprintf('Total frames encoded: %d\n',n_frame);
fprintf('Video Encoding Complete\n');
fprintf('Total encode time for %d frames is %0.3f minutes\n',n_frame,sum(frame_segment)/60);
figure('Name','Encoder performance');
plot(1:n_frame,frame_segment,'-*','LineWidth',1.5); grid on; 
xlabel('Frame number'); ylabel('Time in seconds');


