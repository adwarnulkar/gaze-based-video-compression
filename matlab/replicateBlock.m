function [ label_new ] = replicateBlock( label , blockRatio )
%REPLICATEBLOCK repeats the FB status bit
%   Example
%   label = [1 2 3
%            4 5 6]
%
%   becomes
%
%   label_new = [1 1 2 2 3 3
%                1 1 2 2 3 3
%                4 4 5 5 6 6
%                4 4 5 5 6 6]

height = size(label,1);
width  = size(label,2);
height_new = height*blockRatio;
width_new  = width*blockRatio;

% Matlab Approach
label_new = repelem(label,blockRatio,blockRatio);

% C++ Approach
% label_new = zeros(height_new,width_new);
% for x = 1:blockRatio:height_new
%     for y = 1:blockRatio:width_new
%         
%         for a = 1:1:blockRatio
%             for b = 1:1:blockRatio
%                 label_new(x+a-1,y+b-1) = label(floor(x/blockRatio) + 1,floor(y/blockRatio) + 1);
%             end
%         end
%         
%     end
% end

end

