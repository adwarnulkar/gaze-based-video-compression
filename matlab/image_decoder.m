%% Decode (Image)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu  
% April 2017; Last revision: 12-April-2017
%
% Algorithm
% 
% * Read the encoded file (encode.dct)
% * Extract DCT coefficient of all the 8x8 blocks
% * Perform quantization for each block (divide by Quantization matrix)
% * Perform de-quantization for each block (Multiply by Quant matrix)
% * Perform IDCT on each 8x8 de-quantizaed block
% * Repack all the 8x8 IDCT blocks into image plane
% * Display the image
% * Note: Image quality can be controlled using n1 (Quantization Factor)

clc; clear ; close all;

% Read the encoded image
fileID = fopen('frame.dct');
A = fread(fileID,'int');
fclose(fileID);

% Image parameters
width  = 960;
height = 544; % Note: W and H are multiples are 8
block_height = 8;
block_width  = 8;

% Quantization parameters
n1 = 1; % Quantization factor for foreground
n2 = 1; % Quantization factor for background
Q = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];
 
 % Extracting DCT values
 R_IDCT = zeros(height,width);
 G_IDCT = zeros(height,width);
 B_IDCT = zeros(height,width);

 
 % Outer 2 loops : (Parse through all the 8x8 blocks)
 % Inner 2 loops : (Parse through a single 8x8 block)
 T = 1;
 pointer = 1;
 tic
 for x = 1:1:(height/8)
    for y = 1:1:(width/8)

        isFB = A(pointer);
        pointer = pointer + 1;
        
        if (isFB == 1)
            N = n1; % Use Foreground quantization factor
        else
            N = n2; % Use Background quantization factor
        end
        
        for m = 1:1:block_height
            for n = 1:1:block_width
                R_DCT(m,n) = A(pointer);
                G_DCT(m,n) = A(pointer + block_width * block_height);
                B_DCT(m,n) = A(pointer + 2 * block_width * block_height);
                pointer = pointer + 1;
            end
        end
        
       
        
        % Performing Quantization
        R_Q = round(1.0.*R_DCT ./ (N.*Q));
        G_Q = round(1.0.*G_DCT ./ (N.*Q));
        B_Q = round(1.0.*B_DCT ./ (N.*Q));
        
        % Perform DeQuantization
        R_DQ = R_Q .* N .* Q;
        G_DQ = G_Q .* N .* Q;
        B_DQ = B_Q .* N .* Q;
        
        % Perform IDCT and store
        R_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(R_DQ,[8 8]);
        G_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(G_DQ,[8 8]);
        B_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(B_DQ,[8 8]);
        
        pointer = pointer + 2 * block_width * block_height;
        
    end
 end
toc 

% Display the image
rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
imshow(rgb,'InitialMagnification','fit');