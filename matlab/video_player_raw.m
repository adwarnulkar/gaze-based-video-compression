function [ n_frame , frame_process ] = video_player_raw ( fileID , width , height , fps ,loops)
%VIDEO_PLAYER_RAW Summary of this function goes here
%   Detailed explanation goes here

A = fread(fileID);

% Video parameters
n_frame = length(A)/(width*height*3); % Number of Frames
frame_time_sec = 1/fps;               % Time per frame  
frame_process = zeros(1,n_frame);     % Performance metric  

% RGB plane initialization
R = zeros(height,width);
G = zeros(height,width);
B = zeros(height,width);

pointer = 1;
for k = 1:loops
    for n = 1:1:n_frame
        tic
        for y = 1:1:height
            for x = 1:1:width
                R(y,x) = A(pointer);
                G(y,x) = A(pointer + width*height);
                B(y,x) = A(pointer + 2*width*height);
                pointer = pointer + 1;
            end
        end
    
        R = uint8(R);
        G = uint8(G);
        B = uint8(B);
        rgb = cat(3,R,G,B);
        frame_process(n) = toc;
        pause(frame_time_sec-frame_process);
        imshow(rgb,'InitialMagnification','fit');
        pointer = pointer + 2*width*height;

    end

pointer = 1;
end

end

