function [ label ] = segmentFB( imgGrayI , imgGrayP , mbSize, p , threshold , iteration )
%SEGMENTFB segments the image in Foreground and Background based on motion
%vectors
%   imgGrayI = I frame in Gray level
%   imgGrayP = P frame in Gray level
%   mbSize = Size of the macro block
%   threshold = threshold to perform Annealing
%   iteration = number of iterations to remove noisy foreground

imgGrayI = double(imgGrayI);
imgGrayP = double(imgGrayP);
%p = 7; % Search Area (Should this be provided in the API ??)

% Image parameters
height = size(imgGrayI,1);      % Image height
width  = size(imgGrayI,2);      % Image width
nMb_x = ceil(height / mbSize);  % Number of Macro blocks along height
nMb_y = ceil(width / mbSize);   % Number of Macro blocks along width

%debug: fprintf('Height: %d\n',height);
%debug: fprintf('Width: %d\n',width);

% Exhaustive Search Algorithm
[motionVect, computations] = motionEstSESTSS(imgGrayP,imgGrayI,mbSize,p);
%imgComp = motionComp(imgGrayI, motionVect, mbSize);

% Creating matrix for x and y cordinated of Motion Vector
u = reshape(motionVect(1,:),[nMb_y nMb_x]); u = u';
v = reshape(motionVect(2,:),[nMb_y nMb_x]); v = v';

% Metric Evaluation
M = abs(u.*gradient(u)) + abs(v.*gradient(v));
N = M; % Create a copy of metric for processing
N(N<threshold) = 0;

% Remove metric from the border (since its misleading)
N(1:2,:) = 0 ; N(:,1:2) = 0; N(end-1:end,:) = 0 ; N(:,end-1:end) = 0;

% Create label matrix (1 = Foreground ; 0 = Background)
label = zeros(nMb_x,nMb_y);
label(N>0) = 1;

%debug: fprintf('Size of label is: %d %d\n',size(label,1),size(label,2));

% Perform dilation on labels to remove noisy foreground
temp_label = zeros(nMb_x,nMb_y);
for a = 1:1:iteration
    
    for m = 1:1:nMb_x
        for n = 1:1:nMb_y
            if((m>1) && (n>1) && (m < nMb_x) && (n < nMb_y))
                count = sum(sum(label(m-1:m+1,n-1:n+1)==1));
                if(label(m,n)==1);
                    count = count - 1;
                end
                if(count >= 3)
                    temp_label(m,n) = 1;
                else
                    temp_label(m,n) = 0;
                end
            end
        end
    end
    
    label = temp_label;
    
end

