% Author(s): Aditya Warnulkar , Amit Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu  
% April 2017; Last revision: 12-April-2017

% Code description: This file read the .rgb file and displays the image
% The syntax of RGB file is RRRRR....GGGGG.....BBBBB

clc; clear; close all;

% Read the image file
fileID = fopen('frame_9.rgb');
A = fread(fileID);
fclose(fileID);

% Image parameters
height = 540;
width  = 960;

% Extract R,G,B planes
R = zeros(height,width);
G = zeros(height,width);
B = zeros(height,width);

pointer = 1;
for x = 1:1:height
    for y = 1:1:width
        R(x,y) = A(pointer);
        G(x,y) = A(pointer + width*height);
        B(x,y) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

% Display image
rgb = cat(3,uint8(R),uint8(G),uint8(B));
imshow(rgb,'InitialMagnification','fit');
