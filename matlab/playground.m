%% Video Quantization

clc ; clear ; close all;

% Reading the video
fileID = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');
A = fread(fileID);
loops = 1;


width = 960;
height = 540;
n_frame = length(A)/(width*height*3); % Number of Frames

Q = [16 11 10 16 24 42 51 61 ;
    12 12 14 19 26 58 60 55 ;
    14 13 16 24 40 57 69 56 ;
    14 17 22 29 51 87 80 62 ;
    18 22 37 58 68 109 123 77 ;
    24 35 55 64 81 104 113 92 ;
    49 64 78 87 103 121 120 101 ;
    72 92 95 98 122 100 103 99];

% RGB plane initialization
R = zeros(height,width);
G = zeros(height,width);
B = zeros(height,width);
R_new = zeros(height,width);
G_new = zeros(height,width);
B_new = zeros(height,width);

%R_DCT = zeros(height,width);
%G_DCT = zeros(height,width);
%B_DCT = zeros(height,width);

R_DCT = zeros(8,8);
G_DCT = zeros(8,8);
B_DCT = zeros(8,8);

pointer = 1;
for k = 1:loops
    for n = 1:1:n_frame
        tic
        for y = 1:1:height
            for x = 1:1:width
                R(y,x) = A(pointer);
                G(y,x) = A(pointer + width*height);
                B(y,x) = A(pointer + 2*width*height);
                pointer = pointer + 1;
            end
        end
        
        % Divide the image into 8x8 blocks and perform DCT
        for x = 1:1:80
            for y = 1:1:120
                %R_DCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
                R_DCT = dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
                R_Q = floor(R_DCT ./ Q);
                R_IDCT = idct2(R_Q .* Q , [8 8]);
                R_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(R_IDCT);
            end
        end
        frame_process(n) = toc;
        pause(frame_time_sec-frame_process);
        %imshow(rgb,'InitialMagnification','fit');
        imshow(R_new,'InitialMagnification','fit');
        pointer = pointer + 2*width*height;
        
    end
    
    pointer = 1;
end

%% Encode (Image)

clc; clear ; close all;

% Reading the image
fileID = fopen('image1.rgb');
A = fread(fileID);
width = 352;
height = 288;
block_height = 8;
block_width  = 8;

% Extract image planes from the byte stream
pointer = 1;

for y = 1:1:height
    for x = 1:1:width
        R(y,x) = A(pointer);
        G(y,x) = A(pointer + width*height);
        B(y,x) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

% Display image for debug purpose (comment out later)
rgb = cat(3,uint8(R),uint8(G),uint8(B));
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');

%Line replication logic (If image is not divisible into 8x8 blocks)
% TODO

% Divide image into 8x8 blocks and perform DCT
file_encode = fopen('encode.dct','w');
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        % Red Channel
        R_DCT = int32(dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,R_DCT(m,n),'int');
            end
        end
        
        % Green Channel
        G_DCT = int32(dct2(G(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,G_DCT(m,n),'int');
            end
        end
        
        % Blue Channel
        B_DCT = int32(dct2(B(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,B_DCT(m,n),'int');
            end
        end
        
    end
end

fclose(file_encode);

%% Decode (Image)

clc; clear ; close all;

% Read the encoded image
fileID = fopen('encode.dct');
A = fread(fileID,'int');
fclose(fileID);

% Image parameters
width = 352;
height = 288;
block_height = 8;
block_width  = 8;

% Quantization parameters
n1 = 1; % Quantization factor
Q = [16 11 10 16 24 42 51 61 ;
    12 12 14 19 26 58 60 55 ;
    14 13 16 24 40 57 69 56 ;
    14 17 22 29 51 87 80 62 ;
    18 22 37 58 68 109 123 77 ;
    24 35 55 64 81 104 113 92 ;
    49 64 78 87 103 121 120 101 ;
    72 92 95 98 122 100 103 99];


% Extracting DCT values
R_IDCT = zeros(height,width);
G_IDCT = zeros(height,width);
B_IDCT = zeros(height,width);
pointer = 1;

% Outer 2 loops : (Parse through all the 8x8 blocks)
% Inner 2 loops : (Parse through a single 8x8 block)
tic
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        
        for m = 1:1:block_height
            for n = 1:1:block_width
                R_DCT(m,n) = A(pointer);
                G_DCT(m,n) = A(pointer + block_width * block_height);
                B_DCT(m,n) = A(pointer + 2 * block_width * block_height);
                pointer = pointer + 1;
            end
        end
        
        % Performing Quantization
        R_Q = round(1.0.*R_DCT ./ (n1.*Q));
        G_Q = round(1.0.*G_DCT ./ (n1.*Q));
        B_Q = round(1.0.*B_DCT ./ (n1.*Q));
        
        % Perform DeQuantization
        R_DQ = R_Q .* n1 .* Q;
        G_DQ = G_Q .* n1 .* Q;
        B_DQ = B_Q .* n1 .* Q;
        
        % Perform IDCT and store
        R_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(R_DQ,[8 8]);
        G_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(G_DQ,[8 8]);
        B_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(B_DQ,[8 8]);
        
        pointer = pointer + 2 * block_width * block_height;
        
    end
end

toc
% Display the image
rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
imshow(rgb,'InitialMagnification','fit');
str = sprintf('Quantization Factor: %d',n1);
title (str);

%% Image segmentation (Active Contour algorithm)

clc; clear ; close all;

% Reading the image
width = 960;
height = 540;
block_height = 8;
block_width  = 8;
%fileID = fopen('image1.rgb');
fileID = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');
A = fread(fileID,width*height*3,'uint8');

% Extract image planes from the byte stream
pointer = 1;

for y = 1:1:height
    for x = 1:1:width
        R(y,x) = A(pointer);
        G(y,x) = A(pointer + width*height);
        B(y,x) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

% Display image for debug purpose (comment out later)
rgb = cat(3,uint8(R),uint8(G),uint8(B));
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');

I = rgb2gray(rgb);
figure('Name','Gray Image');
imshow(I,'InitialMagnification','fit');

mask = zeros(size(I));
mask(25:end-25,25:end-25) = 1;
figure('Name','Mask Location');
imshow(mask,'InitialMagnification','fit');
title('Initial Contour Location');

bw = activecontour(I,mask,1000);

figure ('Name','Segmented Image');
imshow(bw,'InitialMagnification','fit');
title('Segmented Image');

%% Frame Reader
% This code extracts a particular frame number from video file and stores
% it as .rgb image file

clc ; clear ; close all;

% Open the video file
video_file = fopen('/Users/adwarnulkar/Downloads/CSCI576_Final_Project/oneperson_960_540.rgb');
A = fread(video_file,'uint8');

% Image parameters
height = 540;
width = 960;
%frame = input('Enter the frame number you want to extract: ');
frame = 9;

% Extract the frame number
img = zeros(1,3*width*height);
pointer = (frame-1)*3*width*height + 1;
img(:) = A(pointer:pointer+3*width*height-1);

% Write to file
str = sprintf('frame_%d.rgb',frame);
outImage = fopen(str,'w');
fwrite(outImage,img);
fclose(outImage);

%% Map 16x16 block to 8x8 block
% Date: 26th April 2017

clc; clear ; close all;

initialBlockSize = 16;
finalBlockSize = 8;
blockRatio = initialBlockSize / finalBlockSize;

height = 2; % Matrix height of 16x16 block frame
width  = 3; % Matrix width of 16x16 block frame

A = randi(10,height,width);

height_new = height*blockRatio; % Matrix height of 8x8 block frame
width_new  = width*blockRatio;  % Matrix width of 8x8 block frame

% Matlab approach
B = repelem(A,blockRatio,blockRatio);

% C++ approach
B = zeros(height_new,width_new);
for x = 1:blockRatio:height_new
    for y = 1:blockRatio:width_new
        
        for a = 1:1:blockRatio
            for b = 1:1:blockRatio
                B(x+a-1,y+b-1) = A(floor(x/blockRatio) + 1,floor(y/blockRatio) + 1);
            end
        end
        
    end
end


%% Map 16x16 block to 8x8 block (Performance measurement)
% Date: 26th April 2017

clc; clear ; close all;

initialBlockSize = 16;
finalBlockSize = 8;


n = 3; % Matrix dimension of 16x16 block
trials = 1000000;

A = randi(10,n);

blockRatio = initialBlockSize / finalBlockSize;
m = sqrt(n^2 * (blockRatio)^2);

% Matlab approach
tic
for t = 1:1:trials
    A = randi(10,n);
    B = repelem(A,blockRatio,blockRatio);
end
time_m = toc;

% C++ approach
tic
for t = 1:1:trials
    A = randi(10,n);
    B = zeros(m);
    for x = 1:blockRatio:m
        for y = 1:blockRatio:m
            
            for a = 1:1:blockRatio
                for b = 1:1:blockRatio
                    B(x+a-1,y+b-1) = A(floor(x/blockRatio) + 1,floor(y/blockRatio) + 1);
                end
            end
            
        end
    end
end
time_c = toc;

% Analysis
fprintf('Time taken by matrix approach : %f (ms)\n',time_m*1000);
fprintf('Time taken by element approach: %f (ms)\n',time_c*1000);

%% Debug: (Cost Function)

clc ; clear ; close all;
mbSize = 16;
i = 33; j = 49;
p = 7;

imgI = double(rgb2gray(imread('./TA/frame_3.ras')));
imgP = double(rgb2gray(imread('./TA/frame_5.ras')));

[row col] = size(imgI);

%vectors = zeros(2,row*col/mbSize^2);
costs = ones(2*p + 1, 2*p +1) * 65537;

for m = -p : p
    for n = -p : p
        refBlkVer = i + m;   % row/Vert co-ordinate for ref block
        refBlkHor = j + n;   % col/Horizontal co-ordinate
        if ( refBlkVer < 1 || refBlkVer+mbSize-1 > row ...
                || refBlkHor < 1 || refBlkHor+mbSize-1 > col)
            continue;
        end
        costs(m+p+1,n+p+1) = costFuncMAD(imgP(i:i+mbSize-1,j:j+mbSize-1), ...
            imgI(refBlkVer:refBlkVer+mbSize-1, refBlkHor:refBlkHor+mbSize-1), mbSize);
    end
end

%% Debug: (Motion Vectors)

clc ; clear ; close all;

mbSize = 16;
p = 7;

imgI = double(rgb2gray(imread('./TA/frame_3.ras')));
imgP = double(rgb2gray(imread('./TA/frame_5.ras')));

[motionVect, computations] = motionEstES(imgP,imgI,mbSize,p);

%% Frame encode (Algorithm 3)

clc ; clear ; close all;

% Segmentation parameters
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;
blockRatio = mbSize / 8;

% Image parameters
width    = 960;
height   = 544;
n_frames = 10;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

tic
imgI = double(rgb2gray(imread('./TA/frame_3.ras')));
imgP = double(rgb2gray(imread('./TA/frame_5.ras')));

label = segmentFB( imgI , imgP , mbSize, p , threshold , iteration );

% Bounding a rectangle around the blob
[labeledImage, numBlobs] = bwlabel(label);
measurements = regionprops(labeledImage, 'BoundingBox');

for k = 1:1:numBlobs
    xLeft = ceil(measurements(k).BoundingBox(1));
    xRight = xLeft + measurements(k).BoundingBox(3);
    yTop = ceil(measurements(k).BoundingBox(2));
    yBottom = yTop + measurements(k).BoundingBox(4);
    label(yTop:yBottom , xLeft:xRight) = 1;
end

% Scaling the label from 16x16 to 8x8 blocks
label_new = replicateBlock( label , blockRatio );

file_label = fopen('./ALGO_3/label_1.dct','w');
fwrite(file_label,label_new,'int16');
fclose(file_label);


img = double(imread('./TA/frame_5.ras'));
R = img(:,:,1);
G = img(:,:,2);
B = img(:,:,3);

R_DCT = zeros(height,width);
G_DCT = zeros(height,width);
B_DCT = zeros(height,width);

for x = 1:8:height
    for y = 1:8:width
        R_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(R(x:x+8-1 , y:y+8-1) , [8 8]));
        G_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(G(x:x+8-1 , y:y+8-1) , [8 8]));
        B_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(B(x:x+8-1 , y:y+8-1) , [8 8]));
    end
end

file_dct   = fopen('./ALGO_3/frame_1.rdct','w');
fwrite(file_dct,R_DCT,'int16');
fclose(file_dct);

file_dct   = fopen('./ALGO_3/frame_1.gdct','w');
fwrite(file_dct,G_DCT,'int16');
fclose(file_dct);

file_dct   = fopen('./ALGO_3/frame_1.bdct','w');
fwrite(file_dct,B_DCT,'int16');
fclose(file_dct);
toc

%% Frame encode looped (Algorithm 3)
% * Read frame by frame
% * Perform blob detection and Box Bounding
% * Write frame by frame

clc ; clear ; close all;

% Segmentation parameters
mbSize = 16;
p = 7;
threshold = 20;
iteration = 3;
blockRatio = mbSize / 8;

% Image parameters
width    = 960;
height   = 544;
n_frame = 30;

nMb_x = ceil(height / mbSize);
nMb_y = ceil(width / mbSize);

% Analysis
frame_read      = zeros(1,n_frame);
frame_segment   = zeros(1,n_frame);
frame_write     = zeros(1,n_frame);
frame_dct       = zeros(1,n_frame);

mkdir('ALGO_3');

for frame = 1:1:n_frame
    tic
    imgI = double(rgb2gray(imread('./TA/frame_3.ras')));
    imgP = double(rgb2gray(imread('./TA/frame_5.ras')));
    frame_read(frame) = toc;
    
    tic
    label = segmentFB( imgI , imgP , mbSize, p , threshold , iteration );
    
    % Bounding a rectangle around the blob
    [labeledImage, numBlobs] = bwlabel(label);
    measurements = regionprops(labeledImage, 'BoundingBox');
    
    for k = 1:1:numBlobs
        xLeft = ceil(measurements(k).BoundingBox(1));
        xRight = xLeft + measurements(k).BoundingBox(3);
        yTop = ceil(measurements(k).BoundingBox(2));
        yBottom = yTop + measurements(k).BoundingBox(4);
        label(yTop:yBottom , xLeft:xRight) = 1;
    end
    
    % Scaling the label from 16x16 to 8x8 blocks
    label_new = replicateBlock( label , blockRatio );
    imshow(label_new,'InitialMagnification','fit');
    pause(1);
    
    frame_segment(frame) = toc;
    
    tic
    img = double(imread('./TA/frame_5.ras'));
    R = img(:,:,1);
    G = img(:,:,2);
    B = img(:,:,3);
    
    R_DCT = zeros(height,width);
    G_DCT = zeros(height,width);
    B_DCT = zeros(height,width);
    
    for x = 1:8:height
        for y = 1:8:width
            R_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(R(x:x+8-1 , y:y+8-1) , [8 8]));
            G_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(G(x:x+8-1 , y:y+8-1) , [8 8]));
            B_DCT(x:x+8-1 , y:y+8-1) = int16(dct2(B(x:x+8-1 , y:y+8-1) , [8 8]));
        end
    end
    
    frame_dct(frame) = toc;
    
    tic
    file_label = fopen('./ALGO_3/label_1.dct','w');
    fwrite(file_label,label_new,'int16');
    fclose(file_label);
    
    file_dct   = fopen('./ALGO_3/frame_1.rdct','w');
    fwrite(file_dct,R_DCT,'int16');
    fclose(file_dct);
    
    file_dct   = fopen('./ALGO_3/frame_1.gdct','w');
    fwrite(file_dct,G_DCT,'int16');
    fclose(file_dct);
    
    file_dct   = fopen('./ALGO_3/frame_1.bdct','w');
    fwrite(file_dct,B_DCT,'int16');
    fclose(file_dct);
    
    frame_write(frame) = toc;
    
end

frame_total = frame_read + frame_segment + frame_dct + frame_write;

%% Frame decode (ALGO 3)

clc ; clear ; close all;

% Video parameters
width  = 960;
height = 544; % Note: W and H are multiples are 8
block_height = 8;
block_width  = 8;
n_frame = 10;

% Quantization parameters
n1 = 1; % Quantization factor for foreground
n2 = 50; % Quantization factor for background
Q = [16 11 10 16 24 42 51 61 ;
    12 12 14 19 26 58 60 55 ;
    14 13 16 24 40 57 69 56 ;
    14 17 22 29 51 87 80 62 ;
    18 22 37 58 68 109 123 77 ;
    24 35 55 64 81 104 113 92 ;
    49 64 78 87 103 121 120 101 ;
    72 92 95 98 122 100 103 99];


% Extracting DCT values
R_IDCT = zeros(height,width);
G_IDCT = zeros(height,width);
B_IDCT = zeros(height,width);

frame_read = zeros(1,n_frame);
frame_process = zeros(1,n_frame);

for frame = 1:1:n_frame
    tic
    file_label = fopen('./ALGO_3/label_1.dct');
    label = fread(file_label,'int16');
    label = reshape(label,height/8,width/8);
    fclose(file_label);
    
    file_dct = fopen('./ALGO_3/frame_1.rdct');
    R_DCT = fread(file_dct,'int16');
    R_DCT = reshape(R_DCT,height,width);
    fclose(file_dct);
    
    file_dct = fopen('./ALGO_3/frame_1.gdct');
    G_DCT = fread(file_dct,'int16');
    G_DCT = reshape(G_DCT,height,width);
    fclose(file_dct);
    
    file_dct = fopen('./ALGO_3/frame_1.bdct');
    B_DCT = fread(file_dct,'int16');
    B_DCT = reshape(B_DCT,height,width);
    fclose(file_dct);
    
    frame_read(frame) = toc;
    
    tic
    for x = 1:8:height
        for y = 1:8:width
            
            
            isFB = label(floor(x/8)+1 , floor(y/8)+1 );
            
            if (isFB == 1)
                N = n1; % Use Foreground quantization factor
            else
                N = n2; % Use Background quantization factor
            end
            
            % Performing Quantization
            R_Q = round(1.0.*R_DCT(x:x+8-1 , y:y+8-1) ./ (N.*Q));
            G_Q = round(1.0.*G_DCT(x:x+8-1 , y:y+8-1) ./ (N.*Q));
            B_Q = round(1.0.*B_DCT(x:x+8-1 , y:y+8-1) ./ (N.*Q));
            
            % Perform DeQuantization
            R_DQ = R_Q .* N .* Q;
            G_DQ = G_Q .* N .* Q;
            B_DQ = B_Q .* N .* Q;
            
            % Perform IDCT and store
            R_IDCT(x:x+8-1 , y:y+8-1) = idct2(R_DQ,[8 8]);
            G_IDCT(x:x+8-1 , y:y+8-1) = idct2(G_DQ,[8 8]);
            B_IDCT(x:x+8-1 , y:y+8-1) = idct2(B_DQ,[8 8]);
            
            
        end
    end
    
    frame_process(frame) = toc;
    %fprintf('Completed Processing frame %d\n',frame);
    
    % Display the image
    rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
    %imshow(rgb,'InitialMagnification','fit');
    %pause(3);
end

%% Decoder using Block processing

clc; clear ; close all;

% Video parameters
width  = 960;
height = 544; % Note: W and H are multiples are 8
n_frame = 10;

% function handles
quant = @(block_struct) quantize(block_struct.data,1);
dequant = @(block_struct) dequantize(block_struct.data,1);
idc = @(block_struct) idct2(block_struct.data,[8 8]);

for frame = 1:1:n_frame
    tic
    file_dct = fopen('./ALGO_3/frame_1.rdct');
    R_DCT = fread(file_dct,'int16');
    R_DCT = reshape(R_DCT,height,width);
    fclose(file_dct);
    
    file_dct = fopen('./ALGO_3/frame_1.gdct');
    G_DCT = fread(file_dct,'int16');
    G_DCT = reshape(G_DCT,height,width);
    fclose(file_dct);
    
    file_dct = fopen('./ALGO_3/frame_1.bdct');
    B_DCT = fread(file_dct,'int16');
    B_DCT = reshape(B_DCT,height,width);
    fclose(file_dct);
    frame_read(frame) = toc;
    
    tic
    R_Q = blockproc(R_DCT,[8 8],quant);
    G_Q = blockproc(G_DCT,[8 8],quant);
    B_Q = blockproc(B_DCT,[8 8],quant);
    
    R_DQ = blockproc(R_Q,[8 8],dequant);
    G_DQ = blockproc(G_Q,[8 8],dequant);
    B_DQ = blockproc(B_Q,[8 8],dequant);
    
    R_IDCT = blockproc(R_DQ,[8 8],idc);
    G_IDCT = blockproc(G_DQ,[8 8],idc);
    B_IDCT = blockproc(B_DQ,[8 8],idc);
    
    rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
    frame_process(frame) = toc;
    %imshow(rgb,'InitialMagnification','fit');
    
end

% Observations
% * No impact on frame process time. 
% * Its the same as blockwise parsing

%% Performance analysis of various motion vector estimations algos

clc; clear ; close all;

load ES_performance.mat;
n_frame = length(frame_read);
frame_segment_ES = frame_segment;
frame_total_ES = frame_total;

load TSS_performance.mat;
frame_segment_TSS = frame_segment;
frame_total_TSS = frame_total;

% load NTSS_performance.mat;
% frame_segment_NTSS = frame_segment;
% frame_total_ES = frame_segment;

figure('Name','Segmentation Time');
plot(1:n_frame,frame_segment_ES,'-*','LineWidth',1.5); grid on; hold on;
plot(1:n_frame,frame_segment_TSS,'-*','LineWidth',1.5);
title('SEGMENTATION TIME (PER FRAME)');


xlabel('Frame number'); ylabel('Time (seconds)');
legend('Extensive Search','Three step search');
hold off;

figure('Name','Total Encode Time');
plot(1:n_frame,frame_total_ES,'-*','LineWidth',1.5); grid on; hold on;
plot(1:n_frame,frame_total_TSS,'-*','LineWidth',1.5);
title('TOTAL ENCODE TIME (PER FRAME)');

xlabel('Frame number'); ylabel('Time (seconds)');
legend('Extensive Search','Three step search');
hold off;

%% Empty file creation for Amit
% Date 29th April 2017

clc ; clear ; close all;

A = [0 0 0];

folder_name = 'trial_empty_files';
mkdir(folder_name);

n_frame = 300;
for k = 1:1:n_frame
    str = sprintf('./%s/frame_%d.dct',folder_name,k);
    fileID = fopen(str,'w');
    fwrite(fileID , A , 'uint8');
end

%% switch test

A = 1;

switch(A)
    case 1
        fprintf('Entered 1\n');
    case 2
        fprintf('Not supposed to enter here');
    case 3
        fprintf('Not supposed to enter here');
end



