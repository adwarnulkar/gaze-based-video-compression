%% Image DCT , Quantization , DeQuantization , IDCT and display (RGB Space)

clc; clear; close all;

% Reading the image
fileID = fopen('image1.rgb');
A = fread(fileID);
width = 352;
height = 288;

%Quantization parameters
n1 = 10;
Q = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];

%Q = n1*ones(8,8);

% Extracting bytes
pointer = 1;

for y = 1:1:height
    for x = 1:1:width
        R(y,x) = A(pointer);
        G(y,x) = A(pointer + width*height);
        B(y,x) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

R = uint8(R);
G = uint8(G);
B = uint8(B);
rgb = cat(3,R,G,B);
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');

% Divide image into 8x8 blocks and perform DCT
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        % Red Channel
        R_DCT = dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        R_Q = round(R_DCT ./ (n1*Q));
        R_IDCT = idct2(R_Q .* n1 .* Q , [8 8]);
        %R_IDCT = idct2(R_DCT , [8 8]);
        R_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(R_IDCT);
        
        % Green Channel
        G_DCT = dct2(G(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        G_Q = round(G_DCT ./ (n1*Q));
        G_IDCT = idct2(G_Q .* n1 .* Q , [8 8]);
        %G_IDCT = idct2(G_DCT , [8 8]);
        G_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(G_IDCT);
        
        % Blue Channel
        B_DCT = dct2(B(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        B_Q = round(B_DCT ./ (n1*Q));
        B_IDCT = idct2(B_Q .* n1 .* Q , [8 8]);
        %B_IDCT = idct2(B_DCT , [8 8]);
        B_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(B_IDCT);
        
    end
end

% Display Quantized image
rgb_new = cat(3,R_new,G_new,B_new);
figure('Name','Quantized Image');
imshow(rgb_new,'InitialMagnification','fit');

%% Image DCT , Quantization , DeQuantization , IDCT and display (YUV Space)

clc; clear; close all;

% Reading the image
fileID = fopen('image1.rgb');
A = fread(fileID);
width = 352;
height = 288;

%Quantization parameters
Q_L = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];

 Q_C = [17 18 24 47 99 99 99 99 ;
        18 21 26 66 99 99 99 99 ;
        24 26 56 99 99 99 99 99 ;
        47 66 99 99 99 99 99 99 ;
        99 99 99 99 99 99 99 99 ;
        99 99 99 99 99 99 99 99 ;
        99 99 99 99 99 99 99 99 ;
        99 99 99 99 99 99 99 99 ];

n1 = 30;
%Q_L = n1*ones(8,8);
%Q_C = n1*ones(8,8);

% Extracting bytes
pointer = 1;

for y = 1:1:height
    for x = 1:1:width
        R(y,x) = A(pointer);
        G(y,x) = A(pointer + width*height);
        B(y,x) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

rgb = cat(3,uint8(R),uint8(G),uint8(B));
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');

% RGB to YUV conversion
Y = zeros(height,width);
U = zeros(height,width);
V = zeros(height,width);
RGB2YUV = [0.299    0.587   0.114;
          -0.14713 -0.28886 0.436; 
          0.615    -0.51499 -0.10001];
YUV2RGB = inv(RGB2YUV);

Y = RGB2YUV(1,1).*R + RGB2YUV(1,2).*G + RGB2YUV(1,3).*B;
U = RGB2YUV(2,1).*R + RGB2YUV(2,2).*G + RGB2YUV(2,3).*B;
V = RGB2YUV(3,1).*R + RGB2YUV(3,2).*G + RGB2YUV(3,3).*B;
        
% Divide image into 8x8 blocks and perform DCT
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        % Red Channel
        Y_DCT = dct2(Y(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        Y_Q = round(Y_DCT ./ Q_L);
        Y_IDCT = idct2(Y_Q .* Q_L , [8 8]);
        %R_IDCT = idct2(R_DCT , [8 8]);
        Y_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = Y_IDCT;
        
        % Green Channel
        U_DCT = dct2(U(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        U_Q = round(U_DCT ./ Q_C);
        U_IDCT = idct2(U_Q .* Q_C , [8 8]);
        %G_IDCT = idct2(G_DCT , [8 8]);
        U_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = U_IDCT;
        
        % Blue Channel
        V_DCT = dct2(V(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        V_Q = round(V_DCT ./ Q_C);
        V_IDCT = idct2(V_Q .* Q_C , [8 8]);
        %B_IDCT = idct2(B_DCT , [8 8]);
        V_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = V_IDCT;
        
    end
end

% YUV to RGB conversion
R_new = zeros(height,width);
G_new = zeros(height,width);
B_new = zeros(height,width);

R_new = YUV2RGB(1,1).*Y_new + YUV2RGB(1,2).*U_new + YUV2RGB(1,3).*V_new;
G_new = YUV2RGB(2,1).*Y_new + YUV2RGB(2,2).*U_new + YUV2RGB(2,3).*V_new;
B_new = YUV2RGB(3,1).*Y_new + YUV2RGB(3,2).*U_new + YUV2RGB(3,3).*V_new;

% Display Quantized image
rgb_new = cat(3,uint8(R_new),uint8(G_new),uint8(B_new));
figure('Name','Quantized Image');
imshow(rgb_new,'InitialMagnification','fit');

%% Animation as to how the image quality degrades with Quantization factor 

clc; clear; close all;

% Reading the image
fileID = fopen('image1.rgb');
A = fread(fileID);
width = 352;
height = 288;

%Quantization parameters
N = 20;
Q = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];

%Q = n1*ones(8,8);

% Extracting bytes
pointer = 1;

for y = 1:1:height
    for x = 1:1:width
        R(y,x) = A(pointer);
        G(y,x) = A(pointer + width*height);
        B(y,x) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

R = uint8(R);
G = uint8(G);
B = uint8(B);
rgb = cat(3,R,G,B);
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');
title ('Original Image');

figure('Name','Quantized Image');
for n1 = 1:1:N
% Divide image into 8x8 blocks and perform DCT
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        % Red Channel
        R_DCT = dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        R_Q = round(R_DCT ./ (n1*Q));
        R_IDCT = idct2(R_Q .* n1 .* Q , [8 8]);
        %R_IDCT = idct2(R_DCT , [8 8]);
        R_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(R_IDCT);
        
        % Green Channel
        G_DCT = dct2(G(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        G_Q = round(G_DCT ./ (n1*Q));
        G_IDCT = idct2(G_Q .* n1 .* Q , [8 8]);
        %G_IDCT = idct2(G_DCT , [8 8]);
        G_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(G_IDCT);
        
        % Blue Channel
        B_DCT = dct2(B(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]);
        B_Q = round(B_DCT ./ (n1*Q));
        B_IDCT = idct2(B_Q .* n1 .* Q , [8 8]);
        %B_IDCT = idct2(B_DCT , [8 8]);
        B_new(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = uint8(B_IDCT);
        
    end
end

% Display Quantized image
rgb_new = cat(3,R_new,G_new,B_new);
imshow(rgb_new,'InitialMagnification','fit');
str = sprintf('Quantization Factor: %d',n1);
title (str);
pause(0.5);
end