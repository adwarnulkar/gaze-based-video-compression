%% Decode (Video)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu  
% April 2017; Last revision: 13-April-2017
%
% Algorithm:
% 
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Convert DCT doefficients from float to int
% * Store the DCT coefficients of all the block in a file (bit stream)
% * This file (encoder.dct) will be used by the decoder.

clc; clear ; close all;

% Read the encoded video
fileID = fopen('algo2_TA_10.dct');
n_frame = 10; % Note : This should be same as number of frame encoded by encoder

% Video parameters
width  = 960;
height = 544; % Note: W and H are multiples are 8
block_height = 8;
block_width  = 8;

% Quantization parameters
n1 = 1; % Quantization factor for foreground
n2 = 50; % Quantization factor for background
Q = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];
 
% Extracting DCT values
R_IDCT = zeros(height,width);
G_IDCT = zeros(height,width);
B_IDCT = zeros(height,width);

% Outer 2 loops : (Parse through all the 8x8 blocks)
% Inner 2 loops : (Parse through a single 8x8 block)
frame_process = zeros(1,n_frame);
for frame = 1:1:n_frame
    A = fread(fileID,120*68*(1 + 3*64),'float32');
    pointer = 1;
    tic
    for x = 1:1:(height/8)
        for y = 1:1:(width/8)
            
            isFB = A(pointer);
            pointer = pointer + 1;
            
            if (isFB == 1)
                N = n1; % Use Foreground quantization factor
            else
                N = n2; % Use Background quantization factor
            end
            
            for m = 1:1:block_height
                for n = 1:1:block_width
                    R_DCT(m,n) = A(pointer);
                    G_DCT(m,n) = A(pointer + block_width * block_height);
                    B_DCT(m,n) = A(pointer + 2 * block_width * block_height);
                    pointer = pointer + 1;
                end
            end
            
            % Performing Quantization
            R_Q = round(1.0.*R_DCT ./ (N.*Q));
            G_Q = round(1.0.*G_DCT ./ (N.*Q));
            B_Q = round(1.0.*B_DCT ./ (N.*Q));
            
            % Perform DeQuantization
            R_DQ = R_Q .* N .* Q;
            G_DQ = G_Q .* N .* Q;
            B_DQ = B_Q .* N .* Q;
            
            % Perform IDCT and store
            R_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(R_DQ,[8 8]);
            G_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(G_DQ,[8 8]);
            B_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(B_DQ,[8 8]);
            
            pointer = pointer + 2 * block_width * block_height;
            
        end
    end
    frame_process(frame) = toc;
    fprintf('Completed Processing frame %d\n',frame);
    
    % Display the image
    rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
    imshow(rgb,'InitialMagnification','fit');
    pause(3);
    
end

fclose(fileID);

fprintf('....\n');
fprintf('Video Decoding Complete\n');

%% Decode (Video) (Read frame by Frame)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu  
% April 2017; Last revision: 13-April-2017
%
% Algorithm:
% 
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Convert DCT doefficients from float to int
% * Store the DCT coefficients of all the block in a file (bit stream)
% * This file (encoder.dct) will be used by the decoder.

clc; clear ; close all;

% Read the encoded video
n_frame = 30; % Note : This should be same as number of frame encoded by encoder

folder_name = 'SESTSS_30';

% Video parameters
width  = 960;
height = 544; % Note: W and H are multiples are 8
block_height = 8;
block_width  = 8;

% Quantization parameters
n1 = 1; % Quantization factor for foreground
n2 = 50; % Quantization factor for background
Q = [16 11 10 16 24 42 51 61 ;
     12 12 14 19 26 58 60 55 ;
     14 13 16 24 40 57 69 56 ;
     14 17 22 29 51 87 80 62 ; 
     18 22 37 58 68 109 123 77 ;
     24 35 55 64 81 104 113 92 ;
     49 64 78 87 103 121 120 101 ; 
     72 92 95 98 122 100 103 99];
 
% Extracting DCT values
R_IDCT = zeros(height,width);
G_IDCT = zeros(height,width);
B_IDCT = zeros(height,width);

% Outer 2 loops : (Parse through all the 8x8 blocks)
% Inner 2 loops : (Parse through a single 8x8 block)
frame_process = zeros(1,n_frame);
for frame = 1:1:n_frame
    file_name = sprintf('./%s/frame_%d.dct',folder_name,frame);
    fileID = fopen(file_name,'r');
    A = fread(fileID,'int16');
    pointer = 1;
    tic
    for x = 1:1:(height/8)
        for y = 1:1:(width/8)
            
            isFB = A(pointer);
            pointer = pointer + 1;
            
            if (isFB == 1)
                N = n1; % Use Foreground quantization factor
            else
                N = n2; % Use Background quantization factor
            end
            
            for m = 1:1:block_height
                for n = 1:1:block_width
                    R_DCT(m,n) = A(pointer);
                    G_DCT(m,n) = A(pointer + block_width * block_height);
                    B_DCT(m,n) = A(pointer + 2 * block_width * block_height);
                    pointer = pointer + 1;
                end
            end
            
            % Performing Quantization
            R_Q = round(1.0.*R_DCT ./ (N.*Q));
            G_Q = round(1.0.*G_DCT ./ (N.*Q));
            B_Q = round(1.0.*B_DCT ./ (N.*Q));
            
            % Perform DeQuantization
            R_DQ = R_Q .* N .* Q;
            G_DQ = G_Q .* N .* Q;
            B_DQ = B_Q .* N .* Q;
            
            % Perform IDCT and store
            R_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(R_DQ,[8 8]);
            G_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(G_DQ,[8 8]);
            B_IDCT(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) = idct2(B_DQ,[8 8]);
            
            pointer = pointer + 2 * block_width * block_height;
            
        end
    end
    frame_process(frame) = toc;
    fprintf('Completed Processing frame %d\n',frame);
    
    % Display the image
    rgb = cat(3,uint8(R_IDCT),uint8(G_IDCT),uint8(B_IDCT));
    imshow(rgb,'InitialMagnification','fit');
    title_str = sprintf('Frame number %d',frame);
    title(title_str);
    pause(3);
    
end

fclose(fileID);

fprintf('....\n');
fprintf('Video Decoding Complete\n');