%% Encode (Image) (Random Foreground Background)
% Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
% University of Southern California , Dept. of Electrical Engineering
% email address: warnulka@usc.edu  
% April 2017; Last revision: 12-April-2017
%
% Algorithm:
% 
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Divide the image into 8x8 blocks
% * Perform DCT on each 8x8 block
% * Convert DCT doefficients from float to int
% * Store the DCT coefficients of all the block in a file (bit stream)
% * This file (encoder.dct) will be used by the decoder.

clc; clear ; close all;

% Reading the image
fileID = fopen('frame_9.rgb');
A = fread(fileID);
fclose(fileID);

% Image parameter
width  = 960;
height = 540;
block_height = 8;
block_width  = 8;

% Extract image planes from the byte stream
pointer = 1;
for x = 1:1:height
    for y = 1:1:width
        R(x,y) = A(pointer);
        G(x,y) = A(pointer + width*height);
        B(x,y) = A(pointer + 2*width*height);
        pointer = pointer + 1;
    end
end

% Display image for debug purpose (comment out later)
rgb = cat(3,uint8(R),uint8(G),uint8(B));
figure('Name','Raw Image');
imshow(rgb,'InitialMagnification','fit');

%Line replication logic (If image is not divisible into 8x8 blocks)
height = 544; % Nearest Multiple of 8
R(541:544,:) = repmat(R(540,:),4,1);
G(541:544,:) = repmat(G(540,:),4,1);
B(541:544,:) = repmat(B(540,:),4,1);

% Segmentation Algorithm goes here
% TODO

% Divide image into 8x8 blocks and perform DCT
S = 1;
file_encode = fopen('frame.dct','w');
for x = 1:1:(height/8)
    for y = 1:1:(width/8)
        
        % Randomly allocate blocks are foreground and background
        % 1 = Foreground (Quantize by n1 in decoder)
        % 0 = Background (QUantize by n2 in decoder)
        if (rand(1) > 0.5)
            FB_type = 1;
        else
            FB_type = 0;
        end
        fwrite(file_encode,FB_type,'int');
        
        % Red Channel
        R_DCT = int32(dct2(R(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,R_DCT(m,n),'int');
            end
        end

        % Green Channel
        G_DCT = int32(dct2(G(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,G_DCT(m,n),'int');
            end
        end        
 
        % Blue Channel
        B_DCT = int32(dct2(B(8*(x-1)+1 : 8*x , 8*(y-1)+1 : 8*y) , [8 8]));
        % Write DCT coefficients into a file
        for m = 1:1:block_height
            for n = 1:1:block_width
                fwrite(file_encode,B_DCT(m,n),'int');
            end
        end        
   
    end
end

fclose(file_encode);
