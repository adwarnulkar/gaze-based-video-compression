# READ ME Template
- The md file can be used as a template to create documents and status reports
- Reference : https://bitbucket.org/tutorials/markdowndemo/src/master/

# Table of contents
1. [Object segmentation](#object_segmentation)
2. [Multi threading in Video Decoder](#multi_threading)
3. [Encoder file format](#encoder_file_format)


## Object Segmentation
- Motion vectors can be treated as a Vector Field
- We need to find the disturbance in the vector field
- Mathematically this is obtained by performing "gradient" operation on x and y components of the motion vectors
![object segmentation](images/segmentation.png)

## Multi threading in Video Decoder
- Read a frame from the encoded file
- Divide the frame into "n" regions and spawn "n" threads
- All the threads run in parallel , but each thread processes blocks within the region sequentially
- Wait until all the threads are complete (Synchronization)
- Display the image
- Refer to video_decoder.py for implementation
![Multithreading in Video Decoder](images/video_decoder_multithreading.png)

## Encoder file format
![Encoder file format](images/encoder_file_format.png)

