#include "stdafx.h"
#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include"opencv2/imgproc.hpp"
#include <math.h>
#include <fstream>
#include "CSC1576_declarations.h"

using namespace cv;
using namespace std;

Encoder::Encoder()
{
	puts("Constructor");
}

Encoder::~Encoder()
{
	puts("Desstructor");
}


void Encoder::DCT_IDCT(const char *argv[], int Height, int Width) {
	/*int quant_Y[8][8] =
	{ { 16,11,10,16,24,40,51,61 },
	{ 12,12,14,19,26,58,60,55 },
	{ 14,13,16,24,40,57,69,56 },
	{ 14,17,22,29,51,87,80,62 },
	{ 18,22,37,56,68,109,103,77 },
	{ 24,35,55,64,81,104,113,92 },
	{ 49,64,78,87,103,121,120,101 },
	{ 72,92,95,98,112,100,103,99 } };*/
	FILE *file;
	errno_t err;
	if ((err = fopen_s(&file, argv[1], "rb") != 0)) {
		cout << "Cannot open file: " << argv[1] << endl;
		exit(1);
	}
	fseek(file, 0, SEEK_END);
	unsigned long len = (unsigned long)ftell(file);///for file length
	printf("%ld\n", len);
	fseek(file, 0, SEEK_SET);//setting the file pointer back to origin
	unsigned char * Video = DynamicMemoryAllocation1d(len);
	//unsigned char * Frames = DynamicMemoryAllocation1d(int(Height*Width*3));	
	int noOfFrames = int(len / unsigned long(Height*Width * 3));
	//printf("no of Frames:%d\n", noOfFrames);
	fread(Video, sizeof(unsigned char), len, file);
	//Padding the image to obtain 8x8 for performing quantization and dequantization
	int newHeight = 0; int newWidth = 0;
	if ((Height % 8 != 0))
	{
		newHeight = ceil((Height / 8.0)) * 8;
	}
	else
		newHeight = Height;

	if ((Width % 8 != 0))
	{
		newWidth = ceil(((double)Width / 8)) * 8;
	}
	else
		newWidth = Width;
	//////////////////////////////////////////////
	int expansion_w = 0; int expansion_h = 0;
	expansion_w = newWidth - Width; expansion_h = newHeight - Height;
	///////////////////////////////////////////
	Mat frame; frame.create(newHeight, newWidth, 16);
	vector<Mat> FrameCatcher(noOfFrames);
	/*Setting the type and Dimensions of the Mat in Vectors
	Consider vector as a container, in which access of the vector can done like an array,
	But it has more flexibility and less constraint on the data storage. Each Mat(frame=363) can be stored at individual locations
	in the vector.
	*/ 
	for (int i = 0; i < noOfFrames; i++) {
		FrameCatcher[i].create(newHeight, newWidth, 16);
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	int FrameCatcherIndex = 0; int ind = 0; 
	Mat interMed = Mat(8, 8, CV_32FC3);//Used for holding 8*8 blocks from the frame for DCT operation
	Mat dctCoeffHolder = Mat(newHeight, newWidth, CV_32FC3);//Not required as such can be eliminated, present just for testing//
	Mat FinalDisplayFrame = Mat(newHeight, newWidth, CV_8UC3);
	namedWindow("Frames", CV_WINDOW_AUTOSIZE); int x = 0; int y = 0; moveWindow("Frames", x, y);//Display parameters
	for (unsigned long z = 0; z < len; z += (Height*Width * 3)) {//number of frames for loop
		ind = 0;
		for (int i = 0; i < Height; i++) {
			for (int j = 0; j < Width; j++) {/////taking one frame out of the sequential data stream
				frame.at<Vec3b>(i, j)[0] = Video[ind + z];
				frame.at<Vec3b>(i, j)[1] = Video[ind + (Height*Width) + z];
				frame.at<Vec3b>(i, j)[2] = Video[ind + (Height*Width * 2) + z];
				ind++;
			}
		}
		//row and column replication
		if (expansion_h > 0) {
			for (int i = Height; i < Height + expansion_h; i++) {
				for (int j = 0; j < Width; j++) {
					frame.at<Vec3b>(i, j)[0] = frame.at<Vec3b>(Height - 1, j)[0];
					frame.at<Vec3b>(i, j)[1] = frame.at<Vec3b>(Height - 1, j)[1];
					frame.at<Vec3b>(i, j)[2] = frame.at<Vec3b>(Height - 1, j)[2];

				}
			}
		}
		if (expansion_w > 0) {
			for (int j = Width; j < Width + expansion_w; j++) {
				for (int i = 0; i <newHeight; i++) {
					frame.at<Vec3b>(i, j)[0] = frame.at<Vec3b>(i, Width - 1)[0];
					frame.at<Vec3b>(i, j)[1] = frame.at<Vec3b>(i, Width - 1)[1];
					frame.at<Vec3b>(i, j)[2] = frame.at<Vec3b>(i, Width - 1)[2];
				}
			}
		}
		///////////////////////////////////////////////////////////////////////
		//frame.data = Frames;
		/*	for (int i = 0; i < 10;i++) {
		cout << int(Frames[i]) << endl;
		}

		cout << "real frame" << endl;*/
		/*for (int i = 538; i < 539;i++) {
		for (int j = 0; j < 5;j++) {
		for (int k = 0; k < 3; k++) {
		cout << int(frame.at<Vec3b>(i,j)[k])<<endl;
		}
		}
		}*/
		frame.convertTo(frame, CV_32FC3);// conversion required as the dct function needs Mat files with float type 
		vector< Mat> dctBlock(3); vector<Mat>dctPlanes(3); Mat dctMerger; vector<Mat>idctPlanes(3); vector< Mat> idctBlock(3);
		Mat displayImageBlock;
		////taking 8*8 block out of the frame
		for (int i = 0; i < newHeight - 8; i += 8) {
			for (int j = 0; j < newWidth - 8; j += 8) {
				for (int k = 0; k < 8; k++) {
					for (int l = 0; l < 8; l++) {
						interMed.at<Vec3f>(k, l)[0] = frame.at<Vec3f>(i + k, j + l)[0];
						interMed.at<Vec3f>(k, l)[1] = frame.at<Vec3f>(i + k, j + l)[1];
						interMed.at<Vec3f>(k, l)[2] = frame.at<Vec3f>(i + k, j + l)[2];
					}
				}
				split(interMed, dctPlanes);//splitting the 8*8 block of 3 channels
				for (int o = 0; o < 3; o++) {
					dct(dctPlanes[o], dctBlock[o]);///dct on 8*8 block
				}
				merge(dctBlock, dctMerger);
				for (int k = 0; k < 8; k++) {
					for (int l = 0; l < 8; l++) {
						dctCoeffHolder.at<Vec3f>(i + k, j + l)[0] = dctMerger.at<Vec3f>(k, l)[0];
						dctCoeffHolder.at<Vec3f>(i + k, j + l)[1] = dctMerger.at<Vec3f>(k, l)[1];
						dctCoeffHolder.at<Vec3f>(i + k, j + l)[2] = dctMerger.at<Vec3f>(k, l)[2];
					}
				}
				split(dctMerger, idctPlanes);
				for (int o = 0; o < 3; o++) {
					idct(idctPlanes[o], idctBlock[o]);
					idctBlock[o].convertTo(idctBlock[o], CV_8UC1);
				}
				merge(idctBlock, displayImageBlock);
				for (int k = 0; k < 8; k++) {
					for (int l = 0; l < 8; l++) {
						FinalDisplayFrame.at<Vec3b>(i + k, j + l)[0] = displayImageBlock.at<Vec3b>(k, l)[0];
						FinalDisplayFrame.at<Vec3b>(i + k, j + l)[1] = displayImageBlock.at<Vec3b>(k, l)[1];
						FinalDisplayFrame.at<Vec3b>(i + k, j + l)[2] = displayImageBlock.at<Vec3b>(k, l)[2];
					}
				}
			}
		}
		/*	for (int i = 0; i < 8;i++) {
		for (int j = 0; j < 8;j++) {
		cout << dctCoeffHolder.at<Vec3f>(i, j)[0] << "\t";
		}
		cout << endl;
		}*/
		cvtColor(FinalDisplayFrame, FrameCatcher[FrameCatcherIndex], CV_RGB2BGR);

		imshow("Frames", FrameCatcher[FrameCatcherIndex]);
		if (waitKey(50) == 27) //wait for 'esc' key press for 30 ms. If 'esc' key is pressed, break loop
		{
			cout << "esc key is pressed by user" << endl;
			break;
		}
		frame.convertTo(frame, CV_8UC3);
		FrameCatcherIndex++;
		cout << FrameCatcherIndex << endl;
	}
	cout << "Done" << endl;

	file = nullptr; delete Video;

	
}
unsigned char* Encoder::DynamicMemoryAllocation1d(unsigned long len) {
	unsigned char * ImagePointer = new unsigned char[len];
	for (unsigned long i = 0; i<len; i++) {
		ImagePointer[i] = 0;
	}
	return ImagePointer;

}
unsigned char* Encoder::DynamicMemoryAllocation1d(int len) {
	unsigned char * ImagePointer = new unsigned char[len];
	for (int i = 0; i<len; i++) {
		ImagePointer[i] = 0;
	}
	return ImagePointer;

}

