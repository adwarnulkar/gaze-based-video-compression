#pragma once
#include "stdafx.h"
#ifndef Declarations_H_
#define Declarations_H_
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include<string>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include <math.h>
#include <fstream>
using namespace cv;
using namespace std;
class Decoder {

	void MouseSampler(int, int, int);
	friend void on_mouse(int ev, int x, int y, int, void* obj);
public:
	void Reader(const char*[], Mat &, Mat &, Mat&, Mat &);
	void Quantizer(const char*[], Mat &, Mat &, Mat&, Mat&, Mat&, Mat &, Mat&, Mat&);
	void DeQuantizer(const char*[], Mat &, Mat &, Mat&, Mat&, Mat&, Mat &, Mat&);
	void IDCT(const char*[], Mat &, Mat &, Mat &, vector<Mat>&);

	void Display(const char*[], vector<Mat> &, vector<Mat>&, Mat &, Mat &, Mat&);
	void BlockNumberIdentifier_Gaze(Mat&);
	unsigned long & FrameLength(const char*[], unsigned long &);
	float * DynamicMemoryAllocation1d(unsigned long);
	unsigned char * DynamicMemoryAllocation1dUC(unsigned long);










};



#endif