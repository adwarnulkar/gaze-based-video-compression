#include "stdafx.h"
#include <stdio.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include"opencv2/imgproc.hpp"
#include <math.h>
#include <fstream>
#include <time.h>
#include "Declarations.h"
#include "Semaphore.h"
#include "BinarySemaphore.h"
using namespace cv;
using namespace std;
mutex m_;
Point coordinates;
////////////////////////////////////////////////
int reader = 1, quant_Counter = 0, deQuant_Counter = 0, Idct_Counter = 0, Display_Counter = 0;
///////////////////////////////////////////////
Semaphore RQ_empty(120 * 68); Semaphore RQ_occupied(0);
Semaphore QDQ_empty(120 * 68); Semaphore QDQ_occupied(0);
Semaphore DQIDCT_empty(120 * 68); Semaphore DQIDCT_occupied(0);
BinSem DisIDCT(0); BinSem DR(0); BinSem IDCTDis(0);
//////////////////For Pause Play Functionality/////////////////////////////////
BinSem DisReadP_P(0); BinSem DisQuantP_P(0); BinSem DisDeQuantP_P(0); BinSem DisIDCTP_P(0);
bool P_P = true;
//////////////////////////////////////////////////////////////////////////////
unsigned long len = 0; int noOfFrames = 447;
//this function is pointing other member function
void on_mouse(int ev, int x, int y, int, void* obj)
{

	Decoder* app = static_cast<Decoder*>(obj);
	if (app)
		app->MouseSampler(ev, x, y);
}
void Decoder::MouseSampler(int event, int x, int y) {

	if (event == EVENT_MOUSEMOVE) {
		coordinates = Point(x, y);
		//cout << "x:" << coordinates.x << "y:" << coordinates.y << endl;
	}
}
void Decoder::Reader(const char*argv[], Mat & dctCoefficientR, Mat & dctCoefficientG, Mat & dctCoefficientB, Mat & BlockTypeMat) {
	FILE *file; String arg;
	errno_t err; time_t start, end;
	int Height = 544; int Width = 960; unsigned long len = FrameLength(argv, len);
	unsigned long arrayLength = len / 4;//TODO: it can be int 16 as well be mindful about that
	float * Video = DynamicMemoryAllocation1d(arrayLength);
	int TotalBlocks = 120 * 68;////(number of blocks per frame);
	int Counter = 0, Block_TypeX = 0, Block_TypeY = 0; int BlockType_Stream = 0;
	time(&start);
	for (reader = 1; reader <= noOfFrames;) {
		arg = argv[1] + to_string(reader) + String(".dct");
		if ((err = fopen_s(&file, arg.c_str(), "rb") != 0)) {
			cout << "Cannot open file: " << arg << endl;
			exit(1);
		}
		fread(Video, sizeof(float), arrayLength, file);
		Block_TypeY = 0; Counter = 0;
		if (atoi(argv[4]) == 1) {
			DR.lock();//unlocking will be done by display and that will be after every Frame
		}
		for (int h = 0; h <= Height - 8; h += 8) {////Height and width incrementing by 8
			Block_TypeX = 0;
			for (int w = 0; w <= Width - 8; w += 8) {
				RQ_empty.lock();////Check for Activation From Quantizer Thread and Decrement Empty Locations
				BlockType_Stream = int(Video[Counter]);////reading the block type from the file stream
				Counter++;//skipping one location as there is our Block Type Info
				for (int m = 0; m < 8; m++) {///////8*8 window loop//////
					for (int n = 0; n < 8; n++) {
						dctCoefficientR.at<float>(h + m, n + w) = Video[Counter];
						dctCoefficientG.at<float>(h + m, n + w) = Video[Counter + 64];
						dctCoefficientB.at<float>(h + m, n + w) = Video[Counter + (64 * 2)];
						Counter++;//reading counter for dctcoeff rgb from one D file, it is also taking care of the Block Type Location on the last iteration///
					}
				}
				Counter += 128;
				//////one block reading finished//////
				if (atoi(argv[4]) == 1) {//// if gaze is ON
					if (!(BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX) == 2)) {
						BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX) = uchar(BlockType_Stream);
					}
				}
				else {//if gaze is OFF
					BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX) = uchar(BlockType_Stream);//then no BlockTypeChange
				}

				RQ_occupied.unlock();//one block done Waking Up the Quantizer Thread
				Block_TypeX++;//Block Column Counter		
			}
			Block_TypeY++;//Block Row Counter
		}//one frame finish here
			fclose(file);
			m_.lock();
		//cout << "Frame no at Reader:" << reader << endl;
			m_.unlock();
			DisReadP_P.lock();//Waiting For Display To Unlock The Reader Thread//
		if (P_P) {
			reader++;//Only if After Verifying it From Display Thread That Pause is Not Pressed We will increment the Frame Count
		}


	}//All Frames Read//
	delete[] Video;//deallocating the Memory
	time(&end);
	cout << "Reader's Job Done in:" << difftime(end, start) / 60.0 << " minutes" << endl;
};
void Decoder::Quantizer(const char*argv[], Mat & dctCoefficientR, Mat & dctCoefficientG, Mat & dctCoefficientB,
	Mat & QuantizedDctCoeffR, Mat & QuantizedDctCoeffG, Mat & QuantizedDctCoeffB, Mat & BlockTypeMat, Mat &BlockTypeMatDQ) {
	int Height = 544; int Width = 960;
	unsigned char quant[8][8] =
	{ { 16,11,10,16,24,40,51,61 },
	{ 12,12,14,19,26,58,60,55 },
	{ 14,13,16,24,40,57,69,56 },
	{ 14,17,22,29,51,87,80,62 },
	{ 18,22,37,56,68,109,103,77 },
	{ 24,35,55,64,81,104,113,92 },
	{ 49,64,78,87,103,121,120,101 },
	{ 72,92,95,98,112,100,103,99 } };
	int n1 = atoi(argv[2]);
	int n2 = atoi(argv[3]); int mainQuant = 0;
	int TotalBlocks = 120 * 68 * noOfFrames;////(number of blocks per frame)*(number of frames);
	int Counter = 0, Block_TypeX = 0, Block_TypeY = 0;
	time_t start, end;
	time(&start);
	for (quant_Counter = 0; quant_Counter < noOfFrames;) {//this loop will run noOfFrames times

		Block_TypeY = 0;
		//The First Two Loops Are Running For 120*68 times/////
		for (int i = 0; i <= Height - 8; i += 8) {
			Block_TypeX = 0;
			for (int j = 0; j <= Width - 8; j += 8) {
				RQ_occupied.lock();////Quantizer thread wake up by Reader////
				QDQ_empty.lock();////Dequantizer will Unlock This Lock//////
								 /////////Here taking care of the foreground and Background Quantization Factor/////////////////////////
				if (BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX) == 1) { mainQuant = n1; }
				else if (BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX) == 0) { mainQuant = n2; }
				else { mainQuant = 1; }
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				if (atoi(argv[4]) == 1 && int(BlockTypeMat.at<uchar>(Block_TypeY, Block_TypeX)) == 2) {
					//if Gaze is ON and also The Mouse Sampling causes the blockType to change to 2 which does not require QUANTIZATION 
					for (int l = 0; l < 8; l++) {
						for (int m = 0; m < 8; m++) {
							QuantizedDctCoeffR.at<int>(i + l, j + m) = int(dctCoefficientR.at<float>(i + l, j + m));
							QuantizedDctCoeffG.at<int>(i + l, j + m) = int(dctCoefficientG.at<float>(i + l, j + m));
							QuantizedDctCoeffB.at<int>(i + l, j + m) = int(dctCoefficientB.at<float>(i + l, j + m));
						}
					}
				}
				else {////else quantize it based on Foreground or Background
					for (int l = 0; l < 8; l++) {
						for (int m = 0; m < 8; m++) {
							QuantizedDctCoeffR.at<int>(i + l, j + m) = round(dctCoefficientR.at<float>(i + l, j + m) / ((mainQuant)*quant[l][m]));
							QuantizedDctCoeffG.at<int>(i + l, j + m) = round(dctCoefficientG.at<float>(i + l, j + m) / ((mainQuant)*quant[l][m]));
							QuantizedDctCoeffB.at<int>(i + l, j + m) = round(dctCoefficientB.at<float>(i + l, j + m) / ((mainQuant)*quant[l][m]));
						}
					}
				}
				//BlockTypeMatDQ = BlockTypeMat.clone();//Be Mindful about this operation
				//////one block reading finished//////
				Block_TypeX++;
				QDQ_occupied.unlock();
				RQ_empty.unlock();
			}
			Block_TypeY++;//Block Row Counter
		}
		m_.lock();
		//cout << "Frame no at Quantizer:" << quant_Counter << endl;
		m_.unlock();
		DisQuantP_P.lock(); // Waiting For Display To Unlock The Quantizer Thread//
		if (P_P) {
			quant_Counter++;//Only if After Verifying it From Display Thread That Pause is Not Pressed We will increment the Frame Count
		}
	}
	time(&end);
	cout << "Quantizer's Job Done in:" << difftime(end, start) / 60.0 << " minutes" << endl;

};
void Decoder::DeQuantizer(const char*argv[], Mat&QuantizedDctCoefficientR, Mat&QuantizedDctCoefficientG, Mat&QuantizedDctCoefficientB,
	Mat & DeQuantizedDctCoefficientR, Mat & DeQuantizedDctCoefficientG, Mat & DeQuantizedDctCoefficientB,
	Mat &BlockTypeMatQD) {
	int Height = 544; int Width = 960;
	unsigned char DeQuant[8][8] =
	{ { 16,11,10,16,24,40,51,61 },
	{ 12,12,14,19,26,58,60,55 },
	{ 14,13,16,24,40,57,69,56 },
	{ 14,17,22,29,51,87,80,62 },
	{ 18,22,37,56,68,109,103,77 },
	{ 24,35,55,64,81,104,113,92 },
	{ 49,64,78,87,103,121,120,101 },
	{ 72,92,95,98,112,100,103,99 } };
	int n1 = atoi(argv[2]);
	int n2 = atoi(argv[3]); int mainQuant = 0;
	int Counter = 0, Block_TypeX = 0, Block_TypeY = 0;
	time_t start, end;
	time(&start);
	for (deQuant_Counter = 0; deQuant_Counter <noOfFrames;) {//this loop will run noOfFrames times
		Block_TypeY = 0;
		for (int i = 0; i <= Height - 8; i += 8) {
			Block_TypeX = 0;
			for (int j = 0; j <= Width - 8; j += 8) {
				QDQ_occupied.lock();////DeQuantizer thread wake up by Quantizer////
				DQIDCT_empty.lock();////IDCT will Unlock This Lock//////
									/////////Here taking care of the foreground and Background DeQuantization Factor/////////////////////////
				if (BlockTypeMatQD.at<uchar>(Block_TypeY, Block_TypeX) == 1) { mainQuant = n1; }
				else if (BlockTypeMatQD.at<uchar>(Block_TypeY, Block_TypeX) == 0) { mainQuant = n2; }
				else { mainQuant = 1; }
				//////////////////////////////////////////////////////////////////////////////////////////////////////
				if (atoi(argv[4]) == 1 && int(BlockTypeMatQD.at<uchar>(Block_TypeY, Block_TypeX)) == 2) {
					//if Gaze is ON and also The Mouse Sampling causes the blockType to change to 2 which does not require DEQUANTIZATION 
					for (int l = 0; l < 8; l++) {
						for (int m = 0; m < 8; m++) {
							DeQuantizedDctCoefficientR.at<float>(i + l, j + m) = float(QuantizedDctCoefficientR.at<int>(i + l, j + m));
							DeQuantizedDctCoefficientG.at<float>(i + l, j + m) = float(QuantizedDctCoefficientG.at<int>(i + l, j + m));
							DeQuantizedDctCoefficientB.at<float>(i + l, j + m) = float(QuantizedDctCoefficientB.at<int>(i + l, j + m));
						}
					}
				}
				else {////else DeQuantize it based on Foreground or Background
					for (int l = 0; l < 8; l++) {
						for (int m = 0; m < 8; m++) {
							DeQuantizedDctCoefficientR.at<float>(i + l, j + m) = float(QuantizedDctCoefficientR.at<int>(i + l, j + m)*(mainQuant)*DeQuant[l][m]);
							DeQuantizedDctCoefficientG.at<float>(i + l, j + m) = float(QuantizedDctCoefficientG.at<int>(i + l, j + m)*(mainQuant)*DeQuant[l][m]);
							DeQuantizedDctCoefficientB.at<float>(i + l, j + m) = float(QuantizedDctCoefficientB.at<int>(i + l, j + m)*(mainQuant)*DeQuant[l][m]);

						}
					}
				}
				//////one block reading finished//////
				Block_TypeX++;
				DQIDCT_occupied.unlock();//again revisit just to check the order//
				QDQ_empty.unlock();
			}
			//cout << endl;
			Block_TypeY++;//Block Row Counter
		}
		m_.lock();
		//cout << "Frame no at DeQuantizer:" << deQuant_Counter << endl;
		m_.unlock();
		DisDeQuantP_P.lock(); // Waiting For Display To Unlock The DeQuantizer Thread//
		if (P_P) {
			deQuant_Counter++;//Only if After Verifying it From Display Thread That Pause is Not Pressed We will increment the Frame Count
		}

	}
	time(&end);
	cout << "DeQuantizer's Job Done in:" << difftime(end, start) / 60.0 << " minutes" << endl;
}
void Decoder::IDCT(const char*argv[], Mat & DeQuantizedDctCoefficientR, Mat & DeQuantizedDctCoefficientG, Mat & DeQuantizedDctCoefficientB,
	vector<Mat> & IDctCoefficient) {
	int Height = 544; int Width = 960;
	vector<Mat> idctPlanes(3); vector<Mat> idctBlock(3); int count = 0;
	time_t start, end;
	time(&start);
	for (Idct_Counter = 0; Idct_Counter < noOfFrames;) {

		if ((count != 0)) {
			IDCTDis.lock();//It will Wait for the indication from the Display that It Has Displayed The Frame Now You Can Edit It//
			count--;
		}
		for (int h = 0; h <= Height - 8; h += 8) {
			for (int w = 0; w <= Width - 8; w += 8) {
				DQIDCT_occupied.lock();//The IDCT thread will be Woken Up by the DeQuantizer Thread/////

				idctPlanes[0] = DeQuantizedDctCoefficientR(Rect(w, h, 8, 8));
				idctPlanes[1] = DeQuantizedDctCoefficientG(Rect(w, h, 8, 8));
				idctPlanes[2] = DeQuantizedDctCoefficientB(Rect(w, h, 8, 8));
				for (int k = 0; k < 3; k++) {
					idct(idctPlanes[k], idctBlock[k]);
					idctBlock[k].copyTo(IDctCoefficient[k](Rect(w, h, 8, 8)));//insertion of the idct block in the frame to be displayed//*/
				}
				////////one block separated//////////
				DQIDCT_empty.unlock();//Indicate the DeQuantizer Thread That I Have Read the Block///	
			}
		}
		m_.lock();
		//cout << "Frame no at IDCT:" << Idct_Counter << endl;
		m_.unlock();
		DisIDCT.unlock();////IDCT of one Frame is Completed Here, Lets Now Call the Display Thread///
		count++;
		DisIDCTP_P.lock();
		if (P_P) {
			Idct_Counter++;//Only if After Verifying it From Display Thread That Pause is Not Pressed We will increment the Frame Count
		}
		//cout << "Frame:"<<i<<"IDCT Done"<< endl;			
	}
	time(&end);
	cout << "IDCT's Job Done in:" << difftime(end, start) / 60.0 << " minutes" << endl;
}
//////////////////////////Display Function//////////////////////////////////////////
void Decoder::Display(const char*argv[], vector<Mat> & Frame, vector<Mat> &FrameCatcher, Mat &InterMed, Mat &FinalDisplayFrame, Mat &BlockMatType) {
	namedWindow("Frames", 1); int x = 0; int y = 0; moveWindow("Frames", x, y);//Display parameters
	if (atoi(argv[4]) == 1) {
		setMouseCallback("Frames", on_mouse, this);
		imshow("Frames", FinalDisplayFrame);//Dummy First Frame
		BlockNumberIdentifier_Gaze(BlockMatType);// Editing the Block Type//
		DR.unlock();/////activating the Reader Thread
		waitKey(30);
	}//// if gaze based is ON then only start sampling*/	
	int count = 0;
	for (Display_Counter = 0; Display_Counter < noOfFrames; ) {
		DisIDCT.lock();//Display Will Get Triggered Only When IDCT Done With One Complete Frame
		Frame[0].convertTo(FrameCatcher[0], CV_8UC1);
		Frame[1].convertTo(FrameCatcher[1], CV_8UC1);
		Frame[2].convertTo(FrameCatcher[2], CV_8UC1);
		merge(FrameCatcher, InterMed);
		cvtColor(InterMed, FinalDisplayFrame, CV_RGB2BGR);
		imshow("Frames", FinalDisplayFrame);
		if (waitKey(50) == 27) //wait for 'esc' key press for 30 ms. If 'esc' key is pressed, break loop
		{//Still Not Sure How Should I Make My Imshow Wait
			cout << "esc key is pressed by user" << endl;
			break;
		}
		if (atoi(argv[4]) == 1) {
			BlockMatType.setTo(Scalar(0));//setting the Block Type Everytime to Zero
			BlockNumberIdentifier_Gaze(BlockMatType);// Editing the Block Type//
			DR.unlock();/////activating the Reader Thread
		}
		if (waitKey(1) == 'p') { //press P to pause video
			cout << "Pausing Video" << endl;
			P_P = false;
			DisReadP_P.unlock(); DisQuantP_P.unlock(); DisDeQuantP_P.unlock(); DisIDCTP_P.unlock();
		}
		else if (waitKey(1) == 'g') {
			cout << "Resuming Video" << endl;
			P_P = true;
			DisReadP_P.unlock(); DisQuantP_P.unlock(); DisDeQuantP_P.unlock(); DisIDCTP_P.unlock();
		}
		else {
			DisReadP_P.unlock(); DisQuantP_P.unlock(); DisDeQuantP_P.unlock(); DisIDCTP_P.unlock();
		}
		IDCTDis.unlock();//waking up the IDCT thread, indicating that now you can Start Editing the Frame As I have Displayed It.
		m_.lock();
		//cout << "Frame no at Display:" << Display_Counter << endl;
		m_.unlock();
		if (P_P) {
			Display_Counter++;//Only if After Verifying it From Display Thread That Pause is Not Pressed We will increment the Frame Count
		}

	}
}
////////////////////Display Function End////////////////////////////////////////////////////////////
void Decoder::BlockNumberIdentifier_Gaze(Mat & BlockMatType) {
	int BlockNumber_x = 0, BlockNumber_y = 0;
	int BlockNumStart_X = 0, BlockNumStart_Y = 0, BlockNumEnd_X = 0, BlockNumEnd_Y = 0;
	//////Identification of the center Block to which the x,y belongs to//////
	BlockNumber_x = coordinates.x / 8; BlockNumber_y = coordinates.y / 8;
	//cout << BlockNumber_x << endl;
	//cout << BlockNumber_y << endl;
	/////////For Loop Initializer/////////////////
	BlockNumStart_X = BlockNumber_x - 3;
	BlockNumEnd_X = BlockNumber_x + 4;
	BlockNumStart_Y = BlockNumber_y - 3;
	BlockNumEnd_Y = BlockNumber_y + 4;
	////////////Taking Care Of the Boundary Condition////////////////////
	if (!(BlockNumStart_X >= 0)) {
		BlockNumStart_X = 0;
	}
	if (!(BlockNumStart_Y >= 0)) {
		BlockNumStart_Y = 0;
	}
	if (!(BlockNumEnd_X <= 119)) {
		BlockNumEnd_X = 119;
	}
	if (!(BlockNumEnd_Y <= 67)) {
		BlockNumEnd_Y = 67;
	}
	for (int bI = BlockNumStart_Y; bI <= BlockNumEnd_Y; bI++) {
		for (int bJ = BlockNumStart_X; bJ <= BlockNumEnd_X; bJ++) {
			BlockMatType.at<uchar>(bI, bJ) = 2;
		}
	}
	//waitKey(0);//to be deleted

}
unsigned char* Decoder::DynamicMemoryAllocation1dUC(unsigned long len) {
	unsigned char * ImagePointer = new unsigned char[len];
	for (unsigned long i = 0; i<len; i++) {
		ImagePointer[i] = 0;
	}
	return ImagePointer;

}
unsigned long & Decoder::FrameLength(const char*argv[], unsigned long &len) {
	FILE *file; String arg; errno_t err;
	arg = argv[1] + to_string(1) + String(".dct");//linking just the first frame and extracting the len from there
	if ((err = fopen_s(&file, arg.c_str(), "rb") != 0)) {
		cout << "Cannot open file: " << arg << endl;
		exit(1);
	}
	fseek(file, 0, SEEK_END);
	len = (unsigned long)ftell(file);//this need not be edited again//
	fseek(file, 0, SEEK_SET);
	fclose(file);
	return len;
}
float* Decoder::DynamicMemoryAllocation1d(unsigned long len) {
	float * ImagePointer = new float[len];
	for (unsigned long i = 0; i<len; i++) {
		ImagePointer[i] = 0;
	}
	return ImagePointer;

}