# Decode video ( With Multi-threading ) 
# Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
# University of Southern California , Dept. of Electrical Engineering
# email address: warnulka@usc.edu  
# April 2017; Last revision: 15-April-2017

# Algorithm
# 
# * Read the encoded file (.dct)
# * Extract DCT coefficient of all the 8x8 blocks
# * Perform quantization for each block (divide by Quantization matrix)
# * Perform de-quantization for each block (Multiply by Quant matrix)
# * Perform IDCT on each 8x8 de-quantizaed block
# * Repack all the 8x8 IDCT blocks into image plane
# * Display the image
# * Note: Image quality can be controlled using n1 (Quantization Factor)

from matplotlib import pyplot as plt
import numpy as np
import numpy.matlib
import cv2
import time
import threading
import thread

class myThread (threading.Thread):
    # Constructor
    def __init__(self,threadID,start_row,end_row,width,n):
        threading.Thread.__init__(self)
        self.threadID = threadID;
        self.start_row = start_row;
        self.end_row = end_row;
        self.width = width;
        self.n = n; # Frame number
    # Thread run method
    def run(self):
        process_frame(self.threadID,self.start_row,
                      self.end_row,self.width,self.n)


def process_frame(threadName,start_row,end_row,width,n):
    thread_tic = time.time();
    global A;
    global R_IDCT;
    global G_IDCT;
    global B_IDCT;
    global n1;
    global n2;
    global Q;
    global thread_time;

    block_width = 8;
    block_height = 8;

    R_DCT = np.empty([block_height,block_width] , dtype=np.float32);
    G_DCT = np.empty([block_height,block_width] , dtype=np.float32);
    B_DCT = np.empty([block_height,block_width] , dtype=np.float32);

    pointer = (start_row/8)*120*(1 + 3*64);

    for x in range (start_row/8,(end_row/8)+1):
        for y in range (0,width/8):
            isFB = A[pointer];
            pointer = pointer + 1;

            if(isFB == 1):
                N = n1; # Use Foreground Quantization factor
            else:
                N = n2; # Use Foreground Quantization factor

            for m in range (0,block_height):
                for n in range (0,block_width):
                    R_DCT[m,n] = A[pointer];
                    G_DCT[m,n] = A[pointer + block_width*block_height];
                    B_DCT[m,n] = A[pointer + 2*block_width*block_height];
                    pointer = pointer + 1;
            
            # Performing Quantization
            R_Q = np.round(1.0*R_DCT / (N*Q));
            G_Q = np.round(1.0*G_DCT / (N*Q));
            B_Q = np.round(1.0*B_DCT / (N*Q));

            # Performing De-Quantization
            R_DQ = R_Q * N * Q;
            G_DQ = G_Q * N * Q;
            B_DQ = B_Q * N * Q;

            # Perform IDCT and store
            R_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(R_DQ) 
            G_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(G_DQ) 
            B_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(B_DQ) 

            pointer = pointer + 2 * block_width * block_height;

    thread_toc = time.time();
    thread_time[threadName,n] = 1000*(thread_toc - thread_tic);
    print "Thread %d takes %d (ms)" %(threadName ,1000*(thread_toc-thread_tic));

############################################################

# Main Routine
# Read the encoded image
fileID = open('random_TA_20.dct','r');
n_frame = 20;

# Video parameters
width  = 960;
height = 544;
block_height = 8;
block_width  = 8;
fps = 30;
frame_time_sec = 1.0/30;

# Initializing IDCT planes (540*960 matrix)
R_IDCT = np.zeros([height,width]);
G_IDCT = np.zeros([height,width]);
B_IDCT = np.zeros([height,width]);

# Quantization parameters
n1 = 1;
n2 = 1;
Q = np.array([[16, 11, 10, 16, 24 , 42  , 51  ,61 ],
     [12, 12, 14, 19, 26 , 58  , 60  ,55 ], 
     [14, 13, 16, 24, 40 , 57  , 69  ,56 ], 
     [14, 17, 22, 29, 51 , 87  , 80  ,62 ],  
     [18, 22, 37, 58, 68 , 109 , 123 ,77 ], 
     [24, 35, 55, 64, 81 , 104 , 113 ,92 ], 
     [49, 64, 78, 87, 103, 121 , 120 ,101], 
     [72, 92, 95, 98, 122, 100 , 103 ,99 ]],np.float32);

# Variables related to threading
thread_num = 2; # Number of threads (height should be divisible by thread_num)

# Variables used for performance analysis
thread_time = np.zeros((thread_num,n_frame));
frame_process_time = np.zeros((1,n_frame));
frame_read_time = np.zeros((1,n_frame));

# Outer 2 loops : (Parse through all the 8x8 blocks)
# Inner 2 loops : (Parse through a single 8x8 block)
block_offset = height / thread_num;
for frame in range (0,n_frame):
    tic = time.time();
    A = np.fromfile(fileID , dtype = np.int32 , count = 120*68*(1 + 3*64));
    read_toc = time.time();

    threads = [];

    # Create new threads
    for t in range(0,thread_num):
        thread = myThread(t,  block_offset*t , block_offset*t + block_offset -
                          1, width,frame)
        thread.start()
        threads.append(thread)

    # Wait for all threads to complete
    for t in threads:
        t.join()

    print "Completed Processing frame %d" % (frame+1);

    # Display Image
    img = cv2.merge((np.uint8(B_IDCT),np.uint8(G_IDCT),np.uint8(R_IDCT)));
    toc = time.time();
    frame_process_time[0,frame] = 1000*(toc-tic);
    frame_read_time[0,frame] = 1000*(read_toc - tic);
    print "Frame process time: %d (ms)\n" % (frame_process_time[0,frame]);
    cv2.imshow('frame',img);
    cv2.waitKey(2);


cv2.destroyAllWindows();
fileID.close();
print "Complete Video decode";

# Performance analysis
x = np.linspace(1,n_frame,n_frame);
plt.figure("Frame Process time");
plt.stem(x, frame_process_time[0,:], '-');
plt.grid(True);
plt.xlabel("Frame Number");
plt.xlabel("Time (ms)");
plt.show();

