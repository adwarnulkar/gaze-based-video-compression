# Video Decoder (without Threading))
# (with pause , play functionality)
# Author(s): Aditya Warnulkar , Amit Nirmala Pandey , Avinash Kaur
# University of Southern California , Dept. of Electrical Engineering
# email address: warnulka@usc.edu  
# May 2017; Last revision: 1-May-2017

# Algorithm
# 
# * Read the encoded files (frame by frame)
# * Detect the blocks which dont need quantization (Gaze blocks)
# * Extract DCT coefficient of all the 8x8 blocks
# * if (block is gaze block)
# *     perform IDCT directly
# * else
# *     perform quantization
# *     perform dequantization
# *     perform IDCT
# * Display the image
# * Note: Image quality can be controlled using n1,n2 (Quantization Factors)

from matplotlib import pyplot as plt
import numpy as np
import numpy.matlib
import cv2
import time

# Function to detect mouse pointer co-ordinates
def mouse(event,x,y,flags,param):
    if (event == cv2.EVENT_MOUSEMOVE):
        #debug: print "Mouse moved at x = %d y = %d" %(x,y);
        global mouse_x;
        global mouse_y;
        mouse_x = x;
        mouse_y = y;

# Function to display nth frame
def display(frame):

    # List of global variables
    global folder_name , width , height , block_width , block_height;
    global n1 , n2 , Q;
    global R_DCT  , G_DCT  , B_DCT;
    global R_IDCT , G_IDCT , B_IDCT;
    global gaze_enable;
    global T;
    
    # Read a frame
    file_name = "%s/frame_%d.dct" %(folder_name,frame);
    fileID = open(file_name,'r');
    A = np.fromfile(fileID , dtype = np.int16 );
    fileID.close();

    # Identifying Gaze blocks
    mouse_loc = np.zeros([68,120]);

    if (gaze_enable):
        X = mouse_y / 8; # block location of gaze
        Y = mouse_x / 8; # block location of gaze
        if (X+8 >= 68):
            startX = X;
            endX   = 68 - 1;
        else:
            startX = X;
            endX   = X + (8 - 1);

        if (Y+8 >= 120):
            startY = Y;
            endY   = 120 - 1;
        else:
            startY = Y;
            endY   = Y + (8 - 1);

        dimX = endX - startX + 1;
        dimY = endY - startY + 1;
        mouse_loc[startX:endX+1][:,startY:endY+1] = np.ones([dimX,dimY]); 

    # Create a frame for display
    pointer = 0;
    for x in range (0,height/8):
        for y in range (0,width/8):
            isFB = A[pointer];
            pointer = pointer + 1;

            if(isFB == 1):
                N = n1; # Use Foreground Quantization factor
            else:
                N = n2; # Use Foreground Quantization factor

            for m in range (0,block_height):
                for n in range (0,block_width):
                    R_DCT[m,n] = A[pointer];
                    G_DCT[m,n] = A[pointer + block_width*block_height];
                    B_DCT[m,n] = A[pointer + 2*block_width*block_height];
                    pointer = pointer + 1;
            
            
            if ((mouse_loc[x,y] == 1) and (gaze_enable == 1)):

                #R_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(1.0*R_DCT) 
                #G_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(1.0*G_DCT) 
                #B_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(1.0*B_DCT) 
                R_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(R_DCT).dot(T.transpose()); 
                G_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(G_DCT).dot(T.transpose()); 
                B_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(B_DCT).dot(T.transpose()); 

            else:
            
                # Performing Quantization
                R_Q = np.round(1.0*R_DCT / (N*Q));
                G_Q = np.round(1.0*G_DCT / (N*Q));
                B_Q = np.round(1.0*B_DCT / (N*Q));

                # Performing De-Quantization
                R_DQ = R_Q * N * Q;
                G_DQ = G_Q * N * Q;
                B_DQ = B_Q * N * Q;

                # Perform IDCT and store
                #R_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(R_DQ) 
                #G_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(G_DQ) 
                #B_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)] = cv2.idct(B_DQ) 

                R_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(R_DQ).dot(T.transpose()); 
                G_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(G_DQ).dot(T.transpose()); 
                B_IDCT[8*x:8*(x+1)][:,8*y:8*(y+1)]=T.dot(B_DQ).dot(T.transpose()); 

            pointer = pointer + 2 * block_width * block_height;

    # Display Image
    img = cv2.merge((np.uint8(B_IDCT),np.uint8(G_IDCT),np.uint8(R_IDCT)));
    toc = time.time();
    #frame_process_time_sec = toc-tic;
    #wait_time = int(1000*(frame_time_sec - frame_process_time_sec));
    #print "Frame process time: %d (ms)\n" % (1000*frame_process_time_sec);
    cv2.imshow('image',img);
    #cv2.waitKey(2);


#################################################################
# MAIN FUNCTION

# GLOBAL VARIABLES

# Reading a file
#folder_name = "/Users/adwarnulkar/Education/CSCI-576 (Multimedia)/Assignments/project_v2/SESTSS_MOVING_483";
folder_name = "/Users/adwarnulkar/Education/CSCI-576 (Multimedia)/Assignments/project/matlab/SESTSS_363";
#folder_name = "/Users/adwarnulkar/Education/CSCI-576 (Multimedia)/Assignments/project/matlab/SESTSS_447";
#folder_name = "/Users/adwarnulkar/Education/CSCI-576 (Multimedia)/Assignments/project/matlab/SESTSS_483";
#folder_name = "/Users/adwarnulkar/Education/CSCI-576 (Multimedia)/Assignments/project/matlab/DEMO_483";

# Video timing parameters
n_frame = 380;
fps = 30;
frame_time_sec = 1.0/30;
delay = 1;

# Video parameters
width        = 960;
height       = 544;
block_height = 8;
block_width  = 8;
playVideo    = 1;

# Quantization parameters
n1 = 1;  # Take input from user
n2 = 50; # Take input from user
Q = np.array([[16, 11, 10, 16, 24 , 42  , 51  ,61 ],
     [12, 12, 14, 19, 26 , 58  , 60  ,55 ], 
     [14, 13, 16, 24, 40 , 57  , 69  ,56 ], 
     [14, 17, 22, 29, 51 , 87  , 80  ,62 ],  
     [18, 22, 37, 58, 68 , 109 , 123 ,77 ], 
     [24, 35, 55, 64, 81 , 104 , 113 ,92 ], 
     [49, 64, 78, 87, 103, 121 , 120 ,101], 
     [72, 92, 95, 98, 122, 100 , 103 ,99 ]],np.float32);

T = np.array([[0.3536,    0.4904 ,   0.4619 ,   0.4157,    0.3536,    0.2778,     0.1913,    0.0975 ],
     [0.3536,    0.4157,    0.1913,   -0.0975,   -0.3536,   -0.4904,   -0.4619,   -0.2778], 
     [0.3536,    0.2778,   -0.1913,   -0.4904,   -0.3536,    0.0975,    0.4619,    0.4157], 
     [0.3536,    0.0975,   -0.4619,   -0.2778,    0.3536,    0.4157,   -0.1913,   -0.4904],  
     [0.3536,   -0.0975,   -0.4619,    0.2778,    0.3536,   -0.4157,   -0.1913,    0.4904], 
     [0.3536,   -0.2778,   -0.1913,    0.4904,   -0.3536,   -0.0975,    0.4619,   -0.4157], 
     [0.3536,   -0.4157,    0.1913,    0.0975,   -0.3536,    0.4904,   -0.4619,    0.2778], 
     [0.3536,   -0.4904,    0.4619,   -0.4157,    0.3536,   -0.2778,    0.1913,   -0.0975]],np.float32);

mouse_x = 0;
mouse_y = 0;
gaze_enable = 1;

# Buffer initialization 
R_IDCT = np.zeros([height,width]);
G_IDCT = np.zeros([height,width]);
B_IDCT = np.zeros([height,width]);

R_DCT = np.empty([block_height,block_width] , dtype=np.int16);
G_DCT = np.empty([block_height,block_width] , dtype=np.int16);
B_DCT = np.empty([block_height,block_width] , dtype=np.int16);

cv2.namedWindow("image");
cv2.setMouseCallback("image",mouse);

while (1):
    frame = 1; temp = 1;

    while (frame <= n_frame):
        print "Displaying frame %d" %(frame);
        
        # Pause the video at the current frame
        if (playVideo == 0):
            display (frame);

        # Resume the video
        if (playVideo == 0 and (cv2.waitKey(delay) == ord('x'))):
            print "Playing Video\n";
            playVideo = 1;

        # Restart the video
        if (playVideo == 0 and (cv2.waitKey(delay) == ord('r'))):
            print "Restarting Video\n";
            playVideo = 1;
            break;

        # Exiting the video
        if ((cv2.waitKey(delay) == ord('e'))):
            print "Exiting Video\n";
            temp = 0;
            break;

        if (playVideo):
            display(frame);

            if(cv2.waitKey(delay) == ord('e')):
                temp = 0;
                break;
            elif (cv2.waitKey(delay) == ord('r')):
                break;

            if (cv2.waitKey(delay) == ord('p')):
                print "Pausing video\n";
                playVideo = 0;

            frame = frame + 1;

    if(temp == 0):
        cv2.destroyAllWindows();
        break;

print "Completed Video Decode";


