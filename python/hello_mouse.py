import cv2

def mouse(event,x,y,flags,param):
    if (event == cv2.EVENT_LBUTTONDOWN):
        print "Left mouse click at x = %d y = %d" %(x,y);
    elif (event == cv2.EVENT_RBUTTONDOWN):
        print "Right mouse click at x = %d y = %d" %(x,y);
    elif (event == cv2.EVENT_MOUSEMOVE):
        print "Mouse moved at x = %d y = %d" %(x,y);


image = cv2.imread('tommy_trojan.jpg');
cv2.namedWindow("image");
cv2.setMouseCallback("image",mouse);
while(1):
    cv2.imshow('image',image)
    k = cv2.waitKey(20) & 0xFF
    if k == ord('b'):
       break; 

cv2.destroyAllWindows()
#cv2.imshow("image",image);
#cv2.waitKey(0);
