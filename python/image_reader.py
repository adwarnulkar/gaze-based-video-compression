# Code to display Image
from matplotlib import pyplot as plt
import numpy as np
import cv2

# Read the image file and extract the bytes
fileID = open('Image1.rgb','rb');
A = np.fromfile(fileID,dtype=np.uint8);
print "The size of the file is %d bytes" %  (A.size);

# Image parameters
width  = 352;
height = 288;

# Extracting image planes
R = np.empty([height ,width] , dtype=np.uint8);
G = np.empty([height ,width] , dtype=np.uint8);
B = np.empty([height ,width] , dtype=np.uint8);

pointer = 0;
for x in range(0,height):
    for y in range(0,width):
        R[x,y] = A[pointer];
        G[x,y] = A[pointer + width*height];
        B[x,y] = A[pointer + 2*width*height];
        pointer = pointer + 1;

# Merging R,G,B planes
img = cv2.merge((B,G,R));
cv2.imshow('frame',img);
cv2.waitKey(3000);
cv2.destroyAllWindows();

# Displaying the image
#plt.imshow(img);
#plt.show();
