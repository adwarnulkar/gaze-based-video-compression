# Decode image

# Algorithm
# 
# * Read the encoded file (encode.dct)
# * Extract DCT coefficient of all the 8x8 blocks
# * Perform quantization for each block (divide by Quantization matrix)
# * Perform de-quantization for each block (Multiply by Quant matrix)
# * Perform IDCT on each 8x8 de-quantizaed block
# * Repack all the 8x8 IDCT blocks into image plane
# * Display the image
# * Note: Image quality can be controlled using n1 (Quantization Factor)

from matplotlib import pyplot as plt
import numpy as np
import cv2

# Read the encoded image
fileID = open('encode.dct','r');
A = np.fromfile(fileID,dtype = np.int32);

#debug: print A[1];

# Image parameters
width  = 352;
height = 288;
block_height = 8;
block_width  = 8;

n1 = 1; # Quantization factor
Q = np.array([[16 , 11 , 10 , 16 , 24  , 42  , 51  , 61], 
             [ 12 , 12 , 14 , 19 , 26  , 58  , 60  , 55], 
             [ 14 , 13 , 16 , 24 , 40  , 57  , 69  , 56], 
             [ 14 , 17 , 22 , 29 , 51  , 87  , 80  , 62],  
             [ 18 , 22 , 37 , 58 , 68  , 109 , 123 , 77], 
             [ 24 , 35 , 55 , 64 , 81  , 104 , 113 , 92], 
             [ 49 , 64 , 78 , 87 , 103 , 121 , 120 , 101],
             [ 72 , 92 , 95 , 98 , 122 , 100 , 103 , 99]]);

#debug: print Q[2,2];

# Extracting the DCT values
R_DCT = np.zeros((block_height,block_width));
G_DCT = np.zeros((block_height,block_width));
B_DCT = np.zeros((block_height,block_width));

R_IDCT = np.zeros((height,width));
G_IDCT = np.zeros((height,width));
B_IDCT = np.zeros((height,width));

# Outer 2 loops : (Parse through all the 8x8 blocks)
# Inner 2 loops : (Parse through a single 8x8 block)
pointer = 0;
for x in range (0,height/8):
    for y in range (0,width/8):
        for m in range (0,block_height):
            for n in range (0,block_width):

                R_DCT[m,n] = A[pointer];
                G_DCT[m,n] = A[pointer + block_height*block_width];
                B_DCT[m,n] = A[pointer + 2*block_height*block_width];
                pointer = pointer + 1;

        # Perform Quantization
        R_Q = np.array(np.round(1.0 * R_DCT / (n1 * Q)));
        G_Q = np.array(np.round(1.0 * G_DCT / (n1 * Q)));
        B_Q = np.array(np.round(1.0 * B_DCT / (n1 * Q)));

        # Perform De-Quantization
        R_DQ = np.array(R_Q * n1 * Q);
        G_DQ = np.array(G_Q * n1 * Q);
        B_DQ = np.array(B_Q * n1 * Q);

        # Perform IDCT and store
        R_IDCT[8*x : 8*(x+1)][:, 8*y : 8*(y+1)] = cv2.idct(R_DQ);
        G_IDCT[8*x : 8*(x+1)][:, 8*y : 8*(y+1)] = cv2.idct(G_DQ);
        B_IDCT[8*x : 8*(x+1)][:, 8*y : 8*(y+1)] = cv2.idct(B_DQ);

        pointer = pointer + 2*block_width*block_height;

#debug: print R_DQ
#debug: print np.uint8(R_IDCT);
#debug: print R_IDCT;

# Merging R,G,B planes
img = cv2.merge((np.uint8(R_IDCT),np.uint8(G_IDCT),np.uint8(B_IDCT)));

# Displaying the image
plt.imshow(img);
plt.show();



